attribute vec3 in_Position;
attribute vec4 in_Colour;
attribute vec2 in_TextureCoord;

varying vec2 v_texcoord;

void main()
{
    gl_Position = gm_Matrices[MATRIX_WORLD_VIEW_PROJECTION] * vec4(in_Position, 1.0);
    v_texcoord = in_TextureCoord;
}
//######################_==_YOYO_SHADER_MARKER_==_######################@~varying vec2 v_texcoord;

uniform float redSepiaForce;
uniform float greenSepiaForce;
uniform float blueSepiaForce;
uniform float redForce;
uniform float greenForce;
uniform float blueForce;
uniform float brightForce;
uniform float contrastForce;

void main()
{ 
    vec4 colour = texture2D(gm_BaseTexture, v_texcoord);
    vec3 sepia = vec3(0.0);
    vec3 rcBoost = vec3(redForce,greenForce,blueForce)/vec3(255,255,255);
    sepia.r = dot(colour.rgb, vec3(0.393,0.769,0.189));
    sepia.g = dot(colour.rgb, vec3(0.349,0.686,0.168));
    sepia.b = dot(colour.rgb, vec3(0.272,0.534,0.131));
    gl_FragColor.rgb = vec3(mix(colour.r,sepia.r, redSepiaForce),mix(colour.g,sepia.g, greenSepiaForce),mix(colour.b,sepia.b, blueSepiaForce))*rcBoost;
    gl_FragColor.a = colour.a;
    vec4 colour2 = gl_FragColor;
    gl_FragColor = vec4( (colour2.rgb+brightForce) * (1.0+contrastForce)/1.0, colour2.a);
    
}
