cameraSpeed = 1;

if (objPlayerCharacter.controllingDrone && !objPlayerCharacter.shouldDrawSelectionCursor){

    var h_spd, v_spd;
    h_spd = 0;
    v_spd = 0;
    
    h_spd +=(keyboard_check(KEY_RIGHT)-keyboard_check(KEY_LEFT)) * spd;
    v_spd +=(keyboard_check(KEY_DOWN)-keyboard_check(KEY_UP)) * spd;
        
    
    h_spd += gamepad_axis_value(0, gp_axislh) * spd;
    v_spd += gamepad_axis_value(0, gp_axislv) * spd;
    
    var _t = instance_create(x, y, objCameraTarget);
    
    if (!MobileCanDoCollision(h_spd * 2, 0, objWallSquare, _t)){
        h_spd = 0;
    }
    
    if (!MobileCanDoCollision(0, v_spd * 2, objWallSquare, _t)){
        v_spd = 0;
    }
    
    with(_t){
        instance_destroy();
    }
    
    //First, collision with the pushable object (small rock)
    MobileDoCollision(h_spd, v_spd, objSmallRock);
    
    if (h_spd > 2)
        sprite_index = sprDroneLeft;
    if (h_spd < -2)
        sprite_index = sprDroneRight;
        
    if (v_spd > 2)
        sprite_index = sprDroneUp;
    if (v_spd < -2)
        sprite_index = sprDroneDown;

    
    //Then, we check for the invisble drone obstacles
    //We set speeds to 0 to avoid using multiple MobileDoCollision
    //This avoids the drone going through the rock, and moving it diagonally.
    if (!MobileCanDoCollision(h_spd, v_spd, parDroneObstacle, id)){
        h_spd = 0;
        v_spd = 0;
    }
    
    //Finally, we push the rock (after testing collisions)
    MobileDoCollisionPushing(h_spd, v_spd, objSmallRock);
    
    view_xview[0] = floor(median(0, room_width - view_wview[0],  objCameraSwitch.x - view_wview[0] / 2));
    view_yview[0] = floor(median(0, room_height - view_hview[0], objCameraSwitch.y - view_hview[0] / 2));
}
