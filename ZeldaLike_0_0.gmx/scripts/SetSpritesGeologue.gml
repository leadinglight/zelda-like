///SetSpritesGeologue

spriteIdleNorth = sprGeologueIdleNorth;
spriteIdleSouth = sprGeologueIdleSouth;
spriteIdleEast = sprGeologueIdleEast;
spriteIdleWest = sprGeologueIdleWest;

spriteTalkingNorth = sprGeologueTalkingNorth;
spriteTalkingSouth = sprGeologueTalkingSouth;
spriteTalkingEast = sprGeologueTalkingEast;
spriteTalkingWest = sprGeologueTalkingWest;
