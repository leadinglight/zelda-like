/*
//  Drone Movement
//  Making the drone do its thing
*/

//set which side the drone will idle (base on the direction of the player)

if (!objPlayerCharacter || objPlayerCharacter.controllingDrone)
    exit;

if (objPlayerCharacter.face == EAST){
    if (shoulderOffset > 0)
        shoulderOffset *= -1;
}
else if (objPlayerCharacter.face == WEST){
    if (shoulderOffset < 0)
        shoulderOffset *= -1;
}

//random movement if he has nothing to do


if (global.droneState == 0){ // Set global.droneState to 0 anywhere to return to idle state
    //spd = 1;
    global.droneState = 1; //1 is the true default state. (But use 0 to reset, it will be easier to add stuff in the future)

}
else if (global.droneState == 1){
    if (objPlayer){
    
        if(abs(gamepad_axis_value(0, gp_axisrh)) > 0.15 || abs(gamepad_axis_value(0, gp_axisrv)) > 0.15){
            spd = 8;
            targetY = objPlayerCharacter.y + 70 * gamepad_axis_value(0, gp_axisrv) - 32;
            targetX = objPlayerCharacter.x + 70 * gamepad_axis_value(0, gp_axisrh);
        } else {
            spd = 4;
            targetY = objPlayerCharacter.y - 70;
            targetX = objPlayerCharacter.x + shoulderOffset;
        }
    }

    if (targetX - error_margin <= x <= targetX + error_margin && targetY - error_margin <= y <= targetY + error_margin && global.droneState == 1){
        global.droneState = 0;
    }
} 
else if (global.droneState == 2){ //Drone charging with player
    targetX = global.droneTargetX;
    targetY = global.droneTargetY;
    
    //smoothing isn't required here, hard-setting the values
    x = global.droneTargetX;
    y = global.droneTargetY;
}


else if (global.droneState == 3){ //Drone following cursor (during connection)
    targetX = objPlayerCharacter.cursorX;
    targetY = objPlayerCharacter.cursorY;
}

//NOT USED---------------------------
else if (global.droneState == 4){ //Drone stasing enemy / object
    targetX = global.frozenTarget.x;
    targetY = global.frozenTarget.y;
}
//-----------------------------------

if (global.droneState != 4 && global.frozenTarget != noone){
    global.frozenTarget.frozen = false;
    global.frozenTarget = noone;
    //If it exists, unfreeze the connected enemy.
    if (global.secondFrozenTarget != noone){
        global.secondFrozenTarget.frozen = false;
        global.secondFrozenTarget = noone;    
    }
}

if ((targetX - x) > 2){
    sprite_index = sprDroneLeft;
    if (objPlayerCharacter.isSelecting)
        sprite_index = sprDroneConnectLeft;
    if (objPlayerCharacter.inFight)
        sprite_index = sprDroneFightLeft;
    }
if ((targetX - x) < -2){
    sprite_index = sprDroneRight;
    if (objPlayerCharacter.isSelecting)
        sprite_index = sprDroneConnectRight;
    if (objPlayerCharacter.inFight)
        sprite_index = sprDroneFightRight;
    }
    
if ((targetY - y) > 2){
    sprite_index = sprDroneUp;
    if (objPlayerCharacter.isSelecting)
        sprite_index = sprDroneConnectUp;
    if (objPlayerCharacter.inFight)
        sprite_index = sprDroneFightUp;
    }
if ((targetY - y) < -2){
    sprite_index = sprDroneDown;
    if (objPlayerCharacter.isSelecting)
        sprite_index = sprDroneConnectDown;
    if (objPlayerCharacter.inFight)
        sprite_index = sprDroneFightDown;
    }


x += (targetX - x) * 0.2 * (spd / 10);
y += (targetY - y) * 0.2 * (spd / 10);


depth = -y;
