///BossScreaming

if(timerScreaming<=2){
    sprite_index = sprBossScreaming;
    image_index = 0;
    alarm[0] = -1;
    if(!audio_is_playing(bossGrowl))
        PlaySound(bossGrowl);
}

image_speed = 0.05*bulletTime;

timerScreaming+=1*bulletTime;

if(gamepad_is_connected(0)){
    gamepad_set_vibration(0, 0.2, 0.2);
    if(!instance_exists(objScreenShake)){
        var screenShake = instance_create(0, 0, objScreenShake);
        screenShake.duration = 110;
        screenShake.shake_amount = 3;
    }
}
    
if(timerScreaming>110){
    Debug("oui");
    gamepad_set_vibration(0, 0, 0);
    alarm[0] = 60;
    currentAttack = BossWaiting;
    timerScreaming = 0;
}

Debug(timerScreaming);
