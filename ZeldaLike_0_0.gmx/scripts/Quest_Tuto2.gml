
// Setup
QuestNew("Tuto2", "Main Quest");
QuestGiver(objMilitaire);

// List requisites
QuestRequisite(CheckQuestInProgress, "Tuto1");

// Start text
QuestDialogueContext(Context.intro);
QuestDialogueBegin(0);
QuestDialogue("Ah te voilà! Comment va ton bras? Tu peux encore l'utiliser?");
QuestDialogueResponse(1,"Oui, il est bien plus fort qu'auparavant...");
QuestDialogueBegin(1);
QuestDialogue("Il peut s'avérer utile... On va voir si tu n'as pas perdu la main. Tu vois le mannequin en forme de monstre? Détruit le avec ton bras.");
QuestDialogueAccept(2, 3, "Si tu veux.", "Attend, je crois que quelqu'un m'appelle.");
QuestDialogueBegin(2);
QuestDialogue("Oui je le veux.");
QuestDialogueBegin(3);
QuestDialogue("... Je n'ai rien entendu...");

// Progress text
QuestDialogueContext(Context.progress);
QuestDialogueBegin(0);
QuestDialogue("Qu'est ce que tu attends? Détruit ce mannequin d'entraînement!");

// End text

// Finish conditions
QuestCondition(CheckExploreArea, markerMannequin);
QuestCondition(EffectFinishQuest);

// Effects

QuestEffect(EffectSpawnObject, markerMannequin, objMannequin);
QuestEffect(EffectStartQuest, "TutoMannequin");

