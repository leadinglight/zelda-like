///BossSideLaser(whichSide)

var whichSide = argument0
var laser;

image_speed = 0.15*bulletTime;

if(sprite_index == sprBossHittingWall || sprite_index == sprBossHittingWallFat && round(image_index)>=image_number)
    sprite_index = sprBossIdleSouth;   

if(round(image_index)==2 && !sound1WallLaser){
    sound1WallLaser = true;
    PlaySound(snd_frappe_mur_boss_1);
}

else if(round(image_index)==6 && sound2WallLaser){
    sound2WallLaser = true;
    PlaySound(snd_frappe_mur_boss_1);
}
    
if(timerSideLaser==0){
    attackDone = false;
}

if(timerSideLaser<=2){
    if(objFaiblesseBoss.inBoss && objFaiblesseBoss.connected)
        sprite_index = sprBossHittingWallFat;
    else
        sprite_index = sprBossHittingWall;
    image_index = 0;
}

else if(timerSideLaser>=30 && !feedbackThrown){
    feedbackThrown = true;
    switch(whichSide){
        case "front" : 
            instance_create(164, 144, objFeedbackWallsFront);
            instance_create(474, 144, objFeedbackWallsFront);
            break;
        case "back" :
            instance_create(204, 608, objFeedbackWallsBack);
            instance_create(448, 608, objFeedbackWallsBack);
            break;
        case "left" :
            instance_create(32, clamp(player.y, 352, 584), objFeedbackWallsLeft);
            break;
        case "right" :
            instance_create(608, clamp(player.y, 352, 584), objFeedbackWallsRight);
            break;
    }
}

else if(timerSideLaser>=70 && !laserThrown){
    laserThrown = true;
    switch(whichSide){
        case "front" : 
            laser = instance_create(164, 144, objBossSideLaser);
            laser.image_angle = 270;
            laser.creator = id;
            laser = instance_create(474, 144, objBossSideLaser);
            laser.image_angle = 270;
            laser.creator = id;
            break;
        case "back" : 
            laser = instance_create(204, 608, objBossSideLaser);
            laser.image_angle = 90;
            laser.creator = id;
            laser = instance_create(448, 608, objBossSideLaser);
            laser.image_angle = 90;
            laser.creator = id;
            break;
        case "left" : 
            laser = instance_create(32, clamp(player.y, 352, 584), objBossSideLaser);
            laser.image_angle = 0;
            laser.creator = id;
            break;
        case "right" : 
            laser = instance_create(608, clamp(player.y, 352, 584), objBossSideLaser);
            laser.image_angle = 180;
            laser.creator = id;
            break;
    }
}

timerSideLaser+=1*bulletTime;

if(attackDone){
    currentAttack = BossWaiting;
    alarm[0] = cdSideLaser;
    timerSideLaser = 0;
    feedbackThrown = false;
    laserThrown = false;
    sound1WallLaser = false;
    sound2WallLaser = false;
}

