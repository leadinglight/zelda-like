///BossOrganeHit(side)

if(timerOrganeHit<=2){
    if(argument0=="left")
        sprite_index = sprBossOrganeHitLeft;
    else
        sprite_index = sprBossOrganeHitRight;
    image_index = 0;
}

image_speed = 0.2*bulletTime;

timerOrganeHit+=1*bulletTime;

if(timerOrganeHit>3 && round(image_index)>=image_number){
    timerOrganeHit = 0;
    currentAttack = BossScreaming;
}
