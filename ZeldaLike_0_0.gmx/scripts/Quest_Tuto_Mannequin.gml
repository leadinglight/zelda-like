
// Setup
QuestNew("TutoMannequin", "MainQuest");
QuestGiver(objMilitaire);

// List requisites
QuestRequisite(CheckQuestComplete, "Tuto2");

// Start text

// Progress text
QuestDialogueContext(Context.progress);
QuestDialogueBegin(0);
QuestDialogue("Détruit ce mannequin!");

// End text


// Finish conditions
QuestCondition(CheckEnemyDefeated, objMannequin);
QuestCondition(EffectFinishQuest);

// Effects

