/// DrawLifeHearts(x, y);
//
//*****************************************************************************

// Position
var _x, _y;
_x = argument0;
_y = argument1;


// Title
//draw_text(_x-88, _y, "----- LIFE -----");

// Draw current life
_x -= 36*7;
//_y += 32;

_i = 0;
repeat (life)
    {
    if (_i = life - 1){
        draw_sprite_ext(sprLifeHeart, 0, _x, _y, image_xscale * objPlayer.currentSize, image_yscale * objPlayer.currentSize, image_angle, image_blend, image_alpha);
    } else {
        draw_sprite(sprLifeHeart, 0, _x, _y);
    }
    
    _i++;
    _x += 36;
    }
    
// Draw remaining empty hearts
repeat (hearts-life)
    {
    draw_sprite(sprLifeHeart, 1, _x, _y);
    _x += 36;
    }
