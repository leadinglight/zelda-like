///BossThrowMatiereNoire(WhichSide)

//var whichSide = argument0;

image_speed = 0.15*bulletTime;

if(sprite_index == sprBossHittingWall && round(image_index)>=image_number)
    sprite_index = sprBossIdleSouth;

if(timerThrowMatiereNoire<=2){
    if(objFaiblesseBoss.inBoss && objFaiblesseBoss.connected)
        sprite_index = sprBossHittingWallFat;
    else
        sprite_index = sprBossHittingWall;
    image_index = 0;
}

if(timerThrowMatiereNoire>=30 && !feedbackThrown){
    feedbackThrown = true;
    switch(argument0){
        case "left" :
            instance_create(32, clamp(player.y, 352, 584), objFeedbackWallsLeft);
            break;
        case "right" :
            instance_create(608, clamp(player.y, 352, 584), objFeedbackWallsRight);
            break;
        case "front" :
            instance_create(164, 144, objFeedbackWallsFront);
            instance_create(474, 144, objFeedbackWallsFront);
            break;
        case "back" :
            var fx = instance_create(56, 588, objFeedbackWallsBack);
            fx.image_angle -= 45;
            fx = instance_create(596, 588, objFeedbackWallsBack);
            fx.image_angle += 45;
            break;
    }
}

repeat(1){
    if(timerThrowMatiereNoire>=70){
        if(bulletTime==1 || irandom(1/bulletTime-1)==0){
            with(instance_create(0, 0, objBossMatiereNoire)){
                switch(argument0){
                    case "left" :
                        x = 32;
                        y= clamp(other.player.y, 384, 584);
                        y += random_range(-15, 15);
                        direction = 0;
                        break;
                    case "right" :
                        x = 576;
                        y = clamp(other.player.y, 384, 584);
                        y += random_range(-15, 15);
                        direction = 180;
                        break;
                    case "front" :
                        x = choose(164, 474);
                        x += random_range(-15, 15);
                        y = 144;
                        direction = 270;
                        break;
                    case "back" :
                        if(irandom(1)){
                            x = 56;
                            y = 588;
                            direction = 45;
                        }
                        else{
                            x = 596;
                            y = 588;
                            direction = 135;
                        }
                        var decalage = random_range(-15, 15);
                        x+=lengthdir_x(decalage, direction+90);
                        y+=lengthdir_y(decalage, direction+90);
                        break;
                }       
            }
        }
    }
}

timerThrowMatiereNoire+=1*bulletTime;

if(timerThrowMatiereNoire>=90){
    currentAttack = BossWaiting;
    alarm[0] = cdThrowMatiereNoire;
    timerThrowMatiereNoire = 0;
    feedbackThrown = false;
}
