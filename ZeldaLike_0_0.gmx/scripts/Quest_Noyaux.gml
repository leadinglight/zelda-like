
// Setup
QuestNew("Quest Noyaux", "Main Quest");
QuestGiver(objCommandante);

// List requisites
QuestRequisite(CheckQuestComplete, "Tuto4");

// Start text


// Progress text
QuestDialogueContext(Context.progress);
QuestDialogueBegin(0);
QuestDialogue("Que fais tu encore là? Va vite explorer la structure que tu as découvert!");


// End text


// Finish conditions
//QuestCondition(CheckExploreArea, markerSkraggleKlaw);
//QuestCondition(EffectFinishQuest);

// Effects
//QuestEffect(EffectSpawnObject, markerSkraggleKlawSpawn, objSkraggleKlaw);
//QuestEffect(EffectStartQuest, "Defeat Skraggle Klaw");
