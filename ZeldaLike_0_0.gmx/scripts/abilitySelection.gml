//Check out Game informations to learn more about IDs.

// New shit: Presising Y on controller changes the ability
if (keyboard_check_pressed(KEY_ABILITYSWITCH) || gamepad_button_check_pressed(0, CONT_ABILITYSWITCH)){
    
    //Annule la connection en cours
    objPlayerCharacter.shouldDrawSelectionCursor = false;
    if (!objPlayerCharacter.controllingDrone){
        objPlayerCharacter.cursorX = objPlayerCharacter.x;
        objPlayerCharacter.cursorY = objPlayerCharacter.y;
    } else {
        objPlayerCharacter.cursorX = objDrone.x;
        objPlayerCharacter.cursorY = objDrone.y;
    }
    if (instance_exists(parEnemy))
        parEnemy.canMove = true;
    objPlayerCharacter.isSelecting = false;
    
    with (objConnectionTarget)
        instance_destroy();
        
    //On lance quand meme les darts pour les objects / enemis qui ont ete selectionnes
    if (!objPlayerCharacter.existingDarts){
        objPlayerCharacter.travellingDarts = ds_list_size(objPlayerCharacter.connecteds);
        
        //instantiate a dart for each target, and set the coordinates
        for (var i = 0; i < ds_list_size(objPlayerCharacter.connecteds); i++){
            var _d = instance_create(objDrone.x, objDrone.y, objDart);
            objPlayerCharacter.existingDarts = true;
            ds_list_add(objPlayerCharacter.darts, _d);
            with (_d){
                targetID = objPlayerCharacter.connecteds[| i].id;
                spd = 6;
            }
        }
    }
        
        
    //Change le pouvoir
    if (objPlayerCharacter.current_ability = 1)
        objPlayerCharacter.current_ability = 3;
    else
        objPlayerCharacter.current_ability = 1;
        
    objPlayer.currentAbility = objPlayerCharacter.current_ability;
    //EXIT AND IGNORE THE FOLLOWING CODE, BECAUSE WE GOT RID OF THE MENU
    exit;
}
