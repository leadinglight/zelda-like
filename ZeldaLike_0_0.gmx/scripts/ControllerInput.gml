//Returns GML booleans
//Checks analog input
//Returns true if treshold is exceeded.
//Call this function with one of the following args:
// "up", "down", "left", "right"

var controllerTreshold = 0.2;


if (argument_count != 1){
    show_debug_message("ERROR IN ControllerInput: EXACTLY 1 ARGUMENT IS REQUIRED. CONTROLLER MOVEMENT DISABLED");
    return 0;
}

switch (argument0){
    case "up":
        if (gamepad_axis_value(0, CONT_UP) < -controllerTreshold)
            return 1;
        return 0;
        break;
    case "down":
        if (gamepad_axis_value(0, CONT_DOWN) > controllerTreshold)
            return 1;
        return 0;
        break;
    case "right":
        if (gamepad_axis_value(0, CONT_RIGHT) > controllerTreshold)
            return 1;
        return 0;
        break;
    case "left":
        if (gamepad_axis_value(0, CONT_LEFT) < -controllerTreshold)
            return 1;
        return 0;
        break;
}
