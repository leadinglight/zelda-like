chargeChargeDuration = 1; //Cooldown before actually charging (cast time)
var _pressed;

//_pressed = (keyboard_check_pressed(KEY_ABILITY) || gamepad_button_check_pressed(0, CONT_ABILITY)) && controlsAble;
_pressed = false //I commented the line above and added thsi line to disable the charge when used as a drone ability (and not while charging the circle thing around the drone)

if ((_pressed && canMove && current_ability = 2 && !charging) || (chargeSignal && canMove && !charging && global.frozenTarget = noone)){

    //we reset the chargeSignal
    chargeSignal = false;

    chargingWithDrone = true;
    //alarm[2] = charge_duration;
    currentChargeSpeed = chargeSpeed;
    global.droneState = 2;
    canCharge = false;
    alarm[3] = chargeChargeDuration;
    //instance_create(x, y, objFXFireReactor1); //ESSAI FX REACTEUR
    global.droneTargetX = x;
    global.droneTargetY = y-32;
}

if (chargingWithDrone && canCharge){
    global.droneTargetX = x;
    global.droneTargetY = y-32;
    
    h_spd = 0;
    v_spd = 0;
    /*if (face == EAST){
        h_spd = currentChargeSpeed;
    }
    if (face == WEST){
        h_spd = -currentChargeSpeed;
    }
    if (face == NORTH){
        v_spd = -currentChargeSpeed;
    }
    if (face == SOUTH){
        v_spd = currentChargeSpeed;
    }*/
    
    
    var directionCharge = point_direction(0, 0, gamepad_axis_value(0, gp_axislh), gamepad_axis_value(0, gp_axislv));
    h_spd = lengthdir_x(currentChargeSpeed, directionCharge);
    v_spd = lengthdir_y(currentChargeSpeed, directionCharge);
    
    //si le joueur bouge pas...
    if (abs(gamepad_axis_value(0, gp_axislh)) < 0.2 && abs(gamepad_axis_value(0, gp_axislv) < 0.2)){
    
        h_spd = 0;
        v_spd = 0;
        
        //...il lest lancé dans la direction dans laquelle il regarde
        switch (face){
            case (NORTH): v_spd -= currentChargeSpeed; break;
            case (SOUTH): v_spd += currentChargeSpeed; break;
            case (EAST):  h_spd += currentChargeSpeed; break;
            case (WEST):  h_spd -= currentChargeSpeed; break;
        }
       
    }
    
    if (currentChargeSpeed > 0)
        currentChargeSpeed -= 0.3;
        
    MobileDoCollisionPushing(h_spd, v_spd, parConnectables);
    MobileDoCollision(h_spd, v_spd, parObstaclePlayer);
    
    //remove old collision mask
    if (instance_exists(objPlayerStrike)){
        with (objPlayerStrike){
            instance_destroy();
        }
    }
    
    //Create collision mask for the charge (because it is an attack that damamges and knockbacks enemies)
    var _newStrikeMask;
        _newStrikeMask = instance_create(x, y, objPlayerStrike);
        _newStrikeMask.face = face;
        _newStrikeMask.spins = 0;
        _newStrikeMask.depth = depth;
        _newStrikeMask.image_speed = image_speed;
        _newStrikeMask.knockback = true;
        with (_newStrikeMask)
            {
            //We use the hitbox sprites now :)
            SetSpriteFromFace(sprHitboxAttackNorth, sprHitboxAttackSouth, sprHitboxAttackEast, sprHitboxAttackWest);
            }

    
    
    if (h_spd = 0 && v_spd = 0){
        //cannot move anymore, charge ended.
        
        //maybe ?
        charging = false;
    }
}
