///BossWaiting

if(objFaiblesseBoss.inBoss && objFaiblesseBoss.connected)
    sprite_index = sprBossIdleSouthFat;
else
    sprite_index = sprBossIdleSouth;
    
image_speed = 0.15;

//RESET TIMERS

timerThrowRock = 0;

timerRectangleAttack = 0;

timerBombardementRock = 0;
rocher1Thrown = false;
rocher2Thrown = false;
sound1Thrown = false;
sound2Thrown = false;
sound3Thrown = false;

timerSideLaser = 0;
feedbackThrown = false;
laserThrown = false;

timerThrowMatiereNoire = 0;

timerHitByRock = 0;

timerOrganeHit = 0;

timerScreaming = 0;

timerDeath = 0;

timerBigTransition = 0;

argumentAttaque0 = "";
argumentAttaque1 = "";
argumentAttaque2 = "";
argumentAttaque3 = "";
