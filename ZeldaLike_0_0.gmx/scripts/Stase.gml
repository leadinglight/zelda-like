var duration = 500;         // Stase duration
var cursorMaxDistance = 100; //Same as the one in connection, go look it up.
var _pressed, _unpressed, _cancel;

//------------- IMPORTANT
//ANCIENNE STASE, ON QUITE TOU C'EST REMPLACE PAR newStase.
//-----------------------
exit;

_pressed = (keyboard_check(KEY_ABILITY) || gamepad_button_check(0, CONT_ABILITY)) && controlsAble;
_selectionKey = (keyboard_check_pressed(vk_space) || gamepad_button_check_pressed(0, CONT_ATTACK) ) && controlsAble;
_unpressed =  (keyboard_check_released(KEY_ABILITY) || gamepad_button_check_released(0, CONT_ABILITY))
_cancel = (keyboard_check_pressed(KEY_CANCEL) || gamepad_button_check_pressed(0, CONT_CANCEL));

//checks if the player exists
if (instance_exists(objPlayerCharacter)){

if (_cancel){
    show_debug_message("exited stasis");
    global.droneState = 1;
    if (global.frozenTarget != noone){
        global.frozenTarget.frozen = false;
        global.frozenTarget = noone;
    }
    if (global.secondFrozenTarget != noone){
        global.secondFrozenTarget.frozen = false;
        global.secondFrozenTarget = noone;
    }
    exit;
}

if (_unpressed && current_ability = 1){
    if (global.droneState != 4)
        global.droneState = 1;
}

if (_pressed && current_ability = 1){
    
    shouldDrawSelectionCursor = true;
    cursorX += (keyboard_check(KEY_RIGHT) + ControllerInput("right")) * cursorSpeed;
    cursorY += (keyboard_check(KEY_DOWN) + ControllerInput("down")) * cursorSpeed;
    cursorX -= (keyboard_check(KEY_LEFT) + ControllerInput("left")) * cursorSpeed;
    cursorY -= (keyboard_check(KEY_UP) + ControllerInput("up")) * cursorSpeed;
    
    //used to freeze enemy position while selecting target
    //parEnemy.canMove = false;
    
    isSelecting = true;

    if (global.droneState != 4)
        global.droneState = 3;
 
    
    if (_selectionKey){

        var foundInst;
        //foundInst = instance_position(cursorX, cursorY, parConnectables)
        //if (foundInst == noone)
        foundInst = instance_nearest(cursorX, cursorY, parEnemy)
        if (foundInst != noone)
            if (point_distance(cursorX, cursorY, foundInst.x, foundInst.y) > cursorMaxDistance)
                foundInst = noone;
        
        if (foundInst != noone){
            if (global.frozenTarget != noone){
                global.frozenTarget.frozen = false;
                global.frozenTarget = noone;
                if (global.secondFrozenTarget != noone){
                    global.secondFrozenTarget.frozen = false;
                    global.secondFrozenTarget = noone;
                }
            }
            //Checks if the stased enemy is part of the connected list.
            if (ds_list_size(objPlayerCharacter.connecteds) = 2){
                if (foundInst == connecteds[| 0]){
                    connecteds[| 1].frozen = true;
                    global.secondFrozenTarget = connecteds[| 1];
                }
                if (foundInst == connecteds[| 1]){
                    connecteds[| 0].frozen = true;
                    global.secondFrozenTarget = connecteds[| 0];
                }
            }
        
            foundInst.frozen = true;
            global.droneTarget = foundInst;
            global.droneState = 4;
            global.frozenTarget = foundInst;
        }

    }
    
} else if (current_ability = 1){

    //else, reset the position of cursor
    shouldDrawSelectionCursor = false;
    if (!controllingDrone){
        cursorX = objPlayerCharacter.x;
        cursorY = objPlayerCharacter.y;
    } else {
        cursorX = objDrone.x;
        cursorY = objDrone.y;
    }
    if (instance_exists(parEnemy))
        parEnemy.canMove = true;
    isSelecting = false;

    //global.droneState = 1;
    
}
/*
if (_pressed && current_ability = 1 && !charging){
    show_debug_message("Stased someone");
    global.droneTarget.frozen = true;
    global.droneTarget.alarm[0] = duration;
    global.droneState = 4;
}*/

}
