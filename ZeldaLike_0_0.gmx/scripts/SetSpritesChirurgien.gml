/// SetSpritesChirurgien();
//
//*****************************************************************************

spriteIdleNorth    = sprBotanisteIdleNorth;
spriteIdleSouth    = sprChirurgienIdleSouth;
spriteIdleEast     = sprChirurgienIdleEast;
spriteIdleWest     = sprChirurgienIdleWest;

// Set initial sprite
sprite_index = spriteIdleSouth;
