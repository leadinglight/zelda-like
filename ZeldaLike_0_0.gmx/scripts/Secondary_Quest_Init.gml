
// Setup
QuestNew("Secondary Quest Init", "Secondary Quest");
QuestGiver(objGeologue);

// List requisites

// Start text
QuestDialogueContext(Context.intro);
QuestDialogueBegin(0);
QuestDialogue("Ah te voilà! On t'attendait. On aurait besoin de ton aide.");
QuestDialogueResponse(1,"D'accord, que dois je faire?")
QuestDialogueBegin(1);
QuestDialogue("On aurait besoin que tu nous rapporte un animal de l'écosystème au Nord Est.");
QuestDialogue("Il nous permettra d'en apprendre beaucoup sur l'écosystème du satellite.");
QuestDialogue("L'un des petits animaux qui vivent dans les arbres dont tu nous a parlé fera parfaitement l'affaire.");
QuestDialogueResponse(2,"Euh… je suis pas contre mais… je l’attrape comment ?")
QuestDialogueBegin(2);
QuestDialogue("Bonne question. Tu nous a bien dit que ces animaux se montrait souvent dans les feuillages des arbres la nuit à la lueur de la planète ?");
QuestDialogueResponse(3, "Oui...");
QuestDialogueBegin(3);
QuestDialogue("Eh bien, on a mit au point une lampe qui imite ce rayonnement.");
QuestDialogue("Tu dois simplement trouver un arbre dans la pénombre! Tu devras l'éclairer avec la lampe.");
QuestDialogue("L'animal sortira et tu pourras l'attraper. Un jeu d'enfant. Attention cependant, il nous le faut vivant!");
QuestDialogueAccept(4, 5, "D'accord je vais voir ce que je peux faire.", "Je n'ai pas vraiment le temps pour l'instant.");
QuestDialogueBegin(4);
QuestDialogue("Super merci! J'installe la lampe sur ton drone.");
QuestDialogue("...");
QuestDialogue("Et voilà le travail! Bonne chasse!");
QuestDialogueBegin(5);
QuestDialogue("Je comprend. Quand tu auras le temps, repasse me voir.");

// Progress text
QuestDialogueContext(Context.progress);
QuestDialogueBegin(0);
QuestDialogue("Tu reviens bredouille?");

// End text

// Finish conditions
//QuestCondition(CheckTalkToNpc, objMilitaire);
//QuestCondition(EffectFinishQuest);

// Effects
