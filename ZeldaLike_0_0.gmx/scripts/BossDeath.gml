///BossDeath

if(timerDeath<=2){
    sprite_index = sprBossDeath;
    image_index = 0;
}

image_speed = 0.15*bulletTime;

timerDeath+=1*bulletTime;

if(round(image_index)>=image_number){
    image_index = -1;
    image_speed = 0;
    image_alpha-=0.05;
}

if(image_alpha<=0)
    instance_destroy();
