///BossBigTransition

timerBigTransition++;

objPlayerCharacter.playerMovable = false;

//JE CRIE-------------------------------------------------------------
if(timerBigTransition==1){
    sprite_index = sprBossScreaming;
    image_index = 0;
    if(gamepad_is_connected(0))
        gamepad_set_vibration(0, 0.2, 0.2);
    if(!instance_exists(objScreenShake)){
        var screenShake = instance_create(0, 0, objScreenShake);
        screenShake.duration = 100;
        screenShake.shake_amount = 3;
    }
    Debug("1");
    image_speed = 0.1;
}

//JE TAPE LES MURS---------------------------------------------------
else if(timerBigTransition==90 && sprite_index==sprBossScreaming){    
    if(instance_exists(objScreenShake)){
        with(objScreenShake){ instance_destroy(); }
    }
    if(gamepad_is_connected(0))
        gamepad_set_vibration(0, 0, 0);
    sprite_index = sprBossHittingWall;
    image_index = 0;
    Debug("2");
}

//LES CAISSES TOMBENT-----------------------------------------------
else if(sprite_index==sprBossHittingWall && round(image_index)==image_number){
    instance_create(448, 384, objRockBossComing);
    instance_create(160, 448, objRockBossComing);
    instance_create(384, 480, objRockBossComing);
    timerBigTransition = 10;
    sprite_index = sprBossIdleSouth;
    image_index = 0;
    Debug("3");
}

//CLIGNOTEMENT OBJROCKBOSS----------------------------------------
else if(sprite_index = sprBossIdleSouth && timerBigTransition>=45 && timerBigTransition<=48){
    timerBigTransition = 50;
    Debug("4");
}

//J'AI MA GLANDE QUI SORT------------------------------------------
else if(timerBigTransition>=200 && sprite_index==sprBossIdleSouth){
    sprite_index = sprBossThrowingGlande;
    image_index = 0;
    Debug("5");
}

//J'AI MA GLANDE QUI DURCIT-------------------------------------------
else if(sprite_index == sprBossThrowingGlande && round(image_index)==image_number){
    sprite_index = sprBossDurcissement;
    image_index = 0;
    Debug("6");
    image_speed = 0.1;
    timerBigTransition = 10;
}

//INSERER CLIGNOTEMENT GLANDE
else if(sprite_index==sprBossDurcissement && round(image_index)==image_number && image_speed!=0){
    with(objFaiblesseBoss){            
        inBoss = false;
        x = positionOutBossX;
        y = positionOutBossY;
        alarm[0] = 240;
    }
    image_index = -1;
    image_speed = 0;
    instance_create(x+4, y+90, objGlandeOrange);
    timerBigTransition = 10;
    Debug("7");
}

//JE RAVALE LA GLANDE-------------------------------------------------------
else if(sprite_index == sprBossDurcissement && timerBigTransition>=220){
    with(objGlandeOrange){ transitionFinished = true; }
    Debug("8");
    image_speed = 0.1;
    currentAttack = BossAfterTransition;
    objPlayerCharacter.playerMovable = true
}
