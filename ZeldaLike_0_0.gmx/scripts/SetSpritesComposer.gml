/// SetSpritesComposer();
//
//*****************************************************************************

//WITH SHIELD-------------------------------------------------------

spriteIdleNorth    = sprComposerIdleNorth;
spriteIdleSouth    = sprComposerIdleSouth;
spriteIdleEast     = sprComposerIdleEast;
spriteIdleWest     = sprComposerIdleWest;

spriteWalkNorth    = sprComposerWalkNorth;
spriteWalkSouth    = sprComposerWalkSouth;
spriteWalkEast     = sprComposerWalkEast;
spriteWalkWest     = sprComposerWalkWest;

spriteAttackLightRightNorth  = sprComposerAttackLightRightNorth;
spriteAttackLightRightSouth  = sprComposerAttackLightRightSouth;
spriteAttackLightRightEast   = sprComposerAttackLightRightEast;
spriteAttackLightRightWest   = sprComposerAttackLightRightWest;

spriteAttackLightLeftNorth  = sprComposerAttackLightLeftNorth;
spriteAttackLightLeftSouth  = sprComposerAttackLightLeftSouth;
spriteAttackLightLeftEast   = sprComposerAttackLightLeftEast;
spriteAttackLightLeftWest   = sprComposerAttackLightLeftWest;

spriteAttackHeavyNorth  = sprComposerAttackHeavyNorth;
spriteAttackHeavySouth  = sprComposerAttackHeavySouth;
spriteAttackHeavyEast   = sprComposerAttackHeavyEast;
spriteAttackHeavyWest   = sprComposerAttackHeavyWest;

spriteStaggerNorth = sprComposerStaggerNorth;
spriteStaggerSouth = sprComposerStaggerSouth;
spriteStaggerEast  = sprComposerStaggerEast;
spriteStaggerWest  = sprComposerStaggerWest;

spriteLosingShieldNorth = sprComposerLosingShieldNorth;
spriteLosingShieldSouth = sprComposerLosingShieldSouth;
spriteLosingShieldEast = sprComposerLosingShieldEast;
spriteLosingShieldWest = sprComposerLosingShieldWest;

//WITHOUT SHIELD------------------------------------------------------

spriteNoShieldIdleNorth    = sprComposerNoShieldIdleNorth;
spriteNoShieldIdleSouth    = sprComposerNoShieldIdleSouth;
spriteNoShieldIdleEast     = sprComposerNoShieldIdleEast;
spriteNoShieldIdleWest     = sprComposerNoShieldIdleWest;

spriteNoShieldWalkNorth    = sprComposerNoShieldWalkNorth;
spriteNoShieldWalkSouth    = sprComposerNoShieldWalkSouth;
spriteNoShieldWalkEast     = sprComposerNoShieldWalkEast;
spriteNoShieldWalkWest     = sprComposerNoShieldWalkWest;

spriteNoShieldAttackHeavyNorth  = sprComposerNoShieldAttackHeavyNorth;
spriteNoShieldAttackHeavySouth  = sprComposerNoShieldAttackHeavySouth;
spriteNoShieldAttackHeavyEast   = sprComposerNoShieldAttackHeavyEast;
spriteNoShieldAttackHeavyWest   = sprComposerNoShieldAttackHeavyWest;

spriteNoShieldStaggerNorth = sprComposerNoShieldStaggerNorth;
spriteNoShieldStaggerSouth = sprComposerNoShieldStaggerSouth;
spriteNoShieldStaggerEast  = sprComposerNoShieldStaggerEast;
spriteNoShieldStaggerWest  = sprComposerNoShieldStaggerWest;

spriteDeathBits    = -1;
spriteDeathNorth   = sprComposerDeathNorth;
spriteDeathSouth   = sprComposerDeathSouth;
spriteDeathEast    = sprComposerDeathEast;
spriteDeathWest    = sprComposerDeathWest;
avatarSprite       = sprAvatarSnowScorpion; //needs to be added

// Set initial sprite
sprite_index = spriteIdleSouth;
