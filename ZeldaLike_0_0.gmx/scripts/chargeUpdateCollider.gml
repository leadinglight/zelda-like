//Update collider (after hitting one while charging)

if (shouldUpdateCollider){
    if (point_distance(global.colliderToUpdateInitialX,
                        global.colliderToUpdateInitialY,
                        global.colliderToUpdate.x,
                        global.colliderToUpdate.y) < global.colliderToUpdate.sprite_width * 2){ //Bump the object at 2 times its width
                            with (global.colliderToUpdate){
                                ConnectedMobileDoCollisionPushing(global.colliderToUpdateHSpeed, global.colliderToUpdateVSpeed, parConnectables);
                            }
                        }
                        else{
                            shouldUpdateCollider = false;
                        }
}
