/// SetSpritesMilitaire();
//
//*****************************************************************************

spriteIdleNorth    = sprMilitaireIdleNorth;
spriteIdleSouth    = sprMilitaireIdleSouth;
spriteIdleEast     = sprMilitaireIdleEast;
spriteIdleWest     = sprMilitaireIdleWest;

// Set initial sprite
sprite_index = spriteIdleEast;
