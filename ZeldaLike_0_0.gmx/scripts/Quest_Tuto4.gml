
// Setup
QuestNew("Tuto4", "Main Quest");
QuestGiver(objCommandante);

// List requisites
QuestRequisite(CheckQuestInProgress, "Tuto1");

// Start text
QuestDialogueContext(Context.intro);
QuestDialogueBegin(0);
QuestDialogue("Bonjour, j'ai une nouvelle tâche pour vous.");
QuestDialogue("Comme vous le savez, les noyaux que vous avez trouvés sont des sources d'énergie. C'est ce que nous voulions trouver.");
QuestDialogue("Ces noyaux sont issus d'un minéral inconnu qui a été raffiné. On doit trouver ces minéraux!");
QuestDialogue("On ne peut pas perdre de temps à fouiller et creuser la zone, on a aucun indice d'où peuvent se trouver ces minéraux.");
QuestDialogue("Cependant, on peut chercher comment cette ancienne civilisation raffinait ces minéraux.");
QuestDialogue("On pourra ensuite sans doute avoir de plus amples informations sur ces minéraux et où ils se trouvent.");
QuestDialogue("Vous devez donc chercher de nouvelles structures de cette ancienne civilisation dans l'espoir de trouver une raffinerie, ou n'importe quoi en relation avec ces minéraux.");
QuestDialogue("Nous avons débroussaillé un chemin au Nord Est, commencez vos recherches par là bas.");
QuestDialogueAccept(1, 2, "Comptez sur moi.", "Je vais me reposer un peu avant de partir.");
QuestDialogueBegin(1);
QuestDialogue("C'est le cas.");
QuestDialogueBegin(2);
QuestDialogue("Quelle bonne idée! Reposons nous au milieu de cette jungle hostile entourée par ces monstres, en attendant que les minéraux nous tombent dessus!");

// Progress text
QuestDialogueContext(Context.progress);
QuestDialogueBegin(0);
QuestDialogue("Nous n'avons pas de temps à perdre. Tout le monde ici compte sur vous, et particulièrement votre soeur.");

// End text

// Finish conditions
QuestCondition(CheckExploreArea, markerTemple2);
QuestCondition(EffectFinishQuest);
// Effects
