if (keyboard_check_pressed(KEY_DRONE) || gamepad_button_check_pressed(0, CONT_DRONE)){
    controllingDrone = !controllingDrone;
    PlaySound(snd_take_drone_control);
    with(objDrone){
        if(place_meeting(x, y, objWallSquare)){
            x = other.x;
            y = other.y;
        }
    }
}

//Stop / cancel drone control if the drone needs to be / is freezing someone.
if (controllingDrone && global.frozenTarget != noone){
    controllingDrone = false;
}
