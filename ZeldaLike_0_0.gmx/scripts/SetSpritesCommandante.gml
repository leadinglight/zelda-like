/// SetSpritesCommandante();
//
//*****************************************************************************

spriteIdleNorth    = sprCommandanteIdleNorth;
spriteIdleSouth    = sprCommandanteIdleSouth;
spriteIdleEast     = sprCommandanteIdleEast;
spriteIdleWest     = sprCommandanteIdleWest;

spriteSwagIdleSouth    = sprCommandanteSwagIdleSouth;

// Set initial sprite
sprite_index = spriteIdleSouth;
