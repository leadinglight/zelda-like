///BossAfterTransition



if(!afterTransition1){
        image_speed = 0.05;
        
    sprite_index = sprBossFaiblesseOutDurcie;
    var rockConnected = false;
    for(var i=0 ; i<instance_number(objRockBoss) ; i++){
        if(instance_find(objRockBoss, i).connected)
            rockConnected = true;
    }
    if(objFaiblesseBoss.connected && rockConnected){
        afterTransition1 = true;
    }
}
else if(!afterTransition2){
    sprite_index = sprBossGlandeDurcieBack;
    image_index = 0;
    with(objFaiblesseBoss){
        inBoss=true;
        x = positionInBossX;
        y = positionInBossY;
    }
    afterTransition2 = true;
    image_speed = 0.1;
}
else{
    sprite_index = sprBossIdleSouthFat;
    image_speed = 0.1;  
    if(!instance_exists(objFlecheOrgane)){
        var flecheOrgane = instance_create(objFaiblesseBoss.positionInBossX+16, objFaiblesseBoss.positionInBossY+16, objFlecheOrgane);
        switch(whichArrow){
            case 0 : 
                flecheOrgane.direction = 0;
                whichArrow++;
                break;
            case 1 :
                flecheOrgane.direction = 90;
                whichArrow++;
                break;
            case 2 : 
                flecheOrgane.direction = 180;
                whichArrow++;
                break;
            case 3 : 
                flecheOrgane.direction = 270;
                whichArrow = 0;
                break;
        }
    } 
}
    


