///BossRectangleAttack()

timerRectangleAttack+=1*bulletTime;
image_speed = 0.15*bulletTime;

if(timerRectangleAttack==1){ 

    //CHECK WHERE THE PLAYER IS-----------------------
    
    with(player){
        if(place_meeting(x, y, objZoneBossLeft))
            other.playerZone = "left";
        else if(place_meeting(x, y, objZoneBossCenter))
            other.playerZone = "center";
        else if(place_meeting(x, y, objZoneBossRight))
            other.playerZone = "right";
    }
    
    //------------------------------------------------

    var rectangle;
    switch(playerZone){
        case "left" : 
            rectangle = instance_create(190, 470, objBossRectangleHitBoxLeft);
            sprite_index = sprBossHittingLeftGround;
            image_index = 0;
            break;
        case "right" : 
            rectangle = instance_create(475, 470, objBossRectangleHitBoxRight);
            sprite_index = sprBossHittingRightGround;
            image_index = 0;
            break;
        case "center" : 
            rectangle = instance_create(332, 470, objBossRectangleHitBoxRight);
            sprite_index = sprBossHittingCenterGround;
            image_index = 0;
            break;
    }
    if(rectangle!=noone){
        rectangle.creator = id;
    }
}

if(timerRectangleAttack>=80){
    currentAttack = BossWaiting;
    alarm[0] = cdRectangleAttack;
    timerRectangleAttack = 0;
}
