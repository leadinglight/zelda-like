///BossBombardementRock

timerBombardementRock+=1*bulletTime;
image_speed = 0.12*bulletTime;

if(timerBombardementRock<2){
    if(objFaiblesseBoss.inBoss && objFaiblesseBoss.connected)
        sprite_index = sprBossShootingFat;
    else
        sprite_index = sprBossShooting;
    image_index = 0;
}

if(round(image_index)==5 && !sound1Thrown){
    PlaySound(snd_throw_boss);
    sound1Thrown = true;
}
else if(round(image_index)==8 && !sound2Thrown){
    PlaySound(snd_throw_boss);
    sound2Thrown = true;
}
else if(round(image_index)==12 && !sound3Thrown){
    PlaySound(snd_throw_boss);
    sound3Thrown = true;
}

if((timerBombardementRock>=45 && !rocher1Thrown) || (timerBombardementRock>=75 && !rocher2Thrown)){
    if(timerBombardementRock<75)
        rocher1Thrown = true;
    else
        rocher2Thrown = true;
    if(irandom(2)==2)
        instance_create(player.x, player.y, objRockShadow);
    else{
        with(instance_create(player.x+40*player.horiSpeed, player.y+40*player.vertSpeed, objRockShadow)){
            if(!place_meeting(x, y, parZoneBoss) || place_meeting(x, y, parObstacle)){
                x = objPlayerCharacter.x;
                y = objPlayerCharacter.y;
            }
        }        
    }
}
else if(timerBombardementRock>=105){
    if(irandom(2)==2)
        instance_create(player.x, player.y, objRockShadow);
    else{
        with(instance_create(player.x+40*player.horiSpeed, player.y+40*player.vertSpeed, objRockShadow)){
            if(!place_meeting(x, y, parZoneBoss) || place_meeting(x, y, parObstacle)){
                x = objPlayerCharacter.x;
                y = objPlayerCharacter.y;
            }
        }        
    }
    //RESET    
    timerBombardementRock = 0;
    currentAttack = BossWaiting;
    alarm[0] = cdBombardementRock;
    rocher1Thrown = false;
    rocher2Thrown = false;
    sound1Thrown = false;
    sound2Thrown = false;
    sound3Thrown = false;
}
