///BossTripleSideLaser

var whichSide = argument0
var laser;

if(sprite_index == sprBossHittingWall && round(image_index)>=image_number)
    sprite_index = sprBossIdleSouth;

if(timerSideLaser==0){
    attackDone = false;
}

if(timerSideLaser<=2){
    sprite_index = sprBossHittingWall;
    image_index = 0;
    image_speed = 0.15;
}


else if(timerSideLaser==60){
    //FRONT
    laser = instance_create(128, 96, objBossSideLaser);
    laser.image_angle = 270;
    laser.creator = id;
    laser = instance_create(512, 96, objBossSideLaser);
    laser.image_angle = 270;
    laser.creator = id;
}

else if(timerSideLaser==120){
    //LEFT 
    laser = instance_create(32, 578, objBossSideLaser);
    laser.image_angle = 0;
    laser.creator = id;
    //RIGHT
    laser = instance_create(608, 318, objBossSideLaser);
    laser.image_angle = 180;
    laser.creator = id;
}

else if(timerSideLaser==180){
    //LEFT 
    laser = instance_create(32, 380, objBossSideLaser);
    laser.image_angle = 0;
    laser.creator = id;
    //RIGHT
    laser = instance_create(608, 520, objBossSideLaser);
    laser.image_angle = 180;
    laser.creator = id;
}

else if(timerSideLaser==240){
    //LEFT 
    laser = instance_create(32, 476, objBossSideLaser);
    laser.image_angle = 0;
    laser.creator = id;
    //RIGHT
    laser = instance_create(608, 424, objBossSideLaser);
    laser.image_angle = 180;
    laser.creator = id;
    
    currentAttack = BossWaiting;
    alarm[0] = cdSideLaser;
    timerSideLaser = 0;
}
timerSideLaser++;
