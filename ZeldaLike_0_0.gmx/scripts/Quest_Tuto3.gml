
// Setup
QuestNew("Tuto3", "Main Quest");
QuestGiver(objMilitaire);

// List requisites
QuestRequisite(CheckQuestComplete, "TutoMannequin");

// Start text
QuestDialogueContext(Context.intro);
QuestDialogueBegin(0);
QuestDialogue("Bien! Quand tu iras t'aventurer dans ces terres inconnues, tu devras utiliser tout ce que tu as sous la main pour t'en sortir")
QuestDialogue("Ton drone peut s'avérer très utile pendant les combats");
QuestDialogue("Ses réacteurs peuvent te pousser pour te frayer des passages à travers la nature, mais tu peux aussi t'en servir contre les ennemis.");
QuestDialogue("En les utilisant, tu pourra esquiver les attaques de tes ennemis, et tu pourras détruire la carapace des créatures les plus résistantes.");
QuestDialogue("Tu peux aussi utiliser ton module de stase sur des objets en mouvements pour les immobiliser, et je suis sûr que ça marche aussi sur les créatures.");
QuestDialogue("Maintenant va voir la commandante, elle te dira ce que tu dois faire.");
QuestDialogueAccept(1,2,"D'accord, je vais voir la commandante suprême...","Elle attendra.");
QuestDialogueBegin(1);
QuestDialogue("... Très drole...");
QuestDialogueBegin(2);
QuestDialogue("N'oublie pas que nous sommes ici en mission.");

// Progress text
QuestDialogueContext(Context.progress);
QuestDialogueBegin(0);
QuestDialogue("Va voir la Commandante.");

// End text



// Finish conditions
QuestCondition(CheckTalkToNpc, objCommandante);
QuestCondition(EffectFinishQuest);
// Effects
