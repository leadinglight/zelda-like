/// SetSpritesDried();
//
//*****************************************************************************

spriteIdleNorth    = sprDriedIdleNorth;
spriteIdleSouth    = sprDriedIdleSouth;
spriteIdleEast     = sprDriedIdleEast;
spriteIdleWest     = sprDriedIdleWest;

spriteWalkNorth    = sprDriedWalkNorth;
spriteWalkSouth    = sprDriedWalkSouth;
spriteWalkEast     = sprDriedWalkEast;
spriteWalkWest     = sprDriedWalkWest;

spritePreparingAttackNorth  = sprDriedPreparingAttackNorth;
spritePreparingAttackSouth  = sprDriedPreparingAttackSouth;
spritePreparingAttackEast   = sprDriedPreparingAttackEast;
spritePreparingAttackWest   = sprDriedPreparingAttackWest;

spriteRunningAttackNorth = sprDriedRunningAttackNorth;
spriteRunningAttackSouth = sprDriedRunningAttackSouth;
spriteRunningAttackEast = sprDriedRunningAttackEast;
spriteRunningAttackWest = sprDriedRunningAttackWest;

spriteEatingNorth = sprDriedEatingNorth;
spriteEatingSouth = sprDriedEatingSouth;
spriteEatingEast  = sprDriedEatingEast;
spriteEatingWest  = sprDriedEatingWest;

spriteStaggerNorth = sprDriedStaggerNorth;
spriteStaggerSouth = sprDriedStaggerSouth;
spriteStaggerEast  = sprDriedStaggerEast;
spriteStaggerWest  = sprDriedStaggerWest;

spriteRepulseNorth = sprDriedRepulseNorth;
spriteRepulseSouth = sprDriedRepulseSouth;
spriteRepulseEast  = sprDriedRepulseEast;
spriteRepulseWest  = sprDriedRepulseWest;

spriteDeathBits    = -1;
spriteDeathNorth   = sprDriedDeathNorth;
spriteDeathSouth   = sprDriedDeathSouth;
spriteDeathEast    = sprDriedDeathEast;
spriteDeathWest    = sprDriedDeathWest;
avatarSprite       = sprAvatarSnowScorpion; //needs to be added

// Set initial sprite
sprite_index = spriteIdleSouth;
