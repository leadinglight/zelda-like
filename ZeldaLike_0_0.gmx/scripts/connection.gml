var cursorMaxDistance = 100; //Error margin for the player to select a target
                             //(The player can be [cursorMaxDistance] pixels form his target but it will be selected anyway
                             //The player isn't required to be exactly on his target.
                             
var _pressed, unpressed, _cancel;

_pressed = (keyboard_check(KEY_ABILITY) || gamepad_button_check(0, CONT_ABILITY)) && controlsAble;
_selectionKey = (keyboard_check_pressed(vk_space) || gamepad_button_check_pressed(0, CONT_ATTACK) ) && controlsAble;

_unpressed =  (keyboard_check_released(KEY_ABILITY) || gamepad_button_check_released(0, CONT_ABILITY))
_cancel = (keyboard_check_pressed(KEY_CANCEL) || gamepad_button_check_pressed(0, CONT_CANCEL))

if (_cancel && current_ability = 3){
    connecteds[| 0].connected = false;
    connecteds[| 1].connected = false;
    ds_list_clear(objPlayerCharacter.connecteds);
}

if (_unpressed && current_ability = 3){
    //reset drone position before and after selecting
    global.droneState = 1;
    //unpause the game
    objGame.isPaused = false;
}

//BULLET TIME SETUP
//if(objPlayerCharacter.watchingObjective==noone)
    bulletTime = 1;


if (_pressed && canMove && current_ability = 3){
    

    //display target on top of enemies that already have been connected
    if (!objDrone.cFlag){
        objDrone.cFlag = true;
        
        for (var i = 0; i < ds_list_size(objPlayerCharacter.connecteds); i++){
        
             if (objPlayerCharacter.connectedMode == 0)
                    instance_create(connecteds[| i].x + connecteds[| i].sprite_width / 2 ,connecteds[| i].y + connecteds[| i].sprite_height / 2, objConnectionTarget);
                else
                    instance_create(connecteds[| i].x, connecteds[| i].y - connecteds[| i].sprite_height / 2, objConnectionTarget);

        }
        
    }

    //pause the game while selecting a target
    //objGame.isPaused = true;
    bulletTime = 0.2;

    shouldDrawSelectionCursor = true;
    cursorX += (keyboard_check(KEY_RIGHT) + ControllerInput("right")) * cursorSpeed;
    cursorY += (keyboard_check(KEY_DOWN) + ControllerInput("down")) * cursorSpeed;
    cursorX -= (keyboard_check(KEY_LEFT) + ControllerInput("left")) * cursorSpeed;
    cursorY -= (keyboard_check(KEY_UP) + ControllerInput("up")) * cursorSpeed;
    //parEnemy.canMove = false;
    isSelecting = true;

    //The drone should no longer be following the cursor, commenting next line.
    //global.droneState = 3;

    if (_selectionKey){
    
        //reset the darts
        objPlayerCharacter.existingDarts = false;
        ds_list_clear(objPlayerCharacter.darts)
        
        var foundInst = noone;
        
        foundInst = instance_nearest(cursorX, cursorY, parConnectables)
        if (foundInst != noone)
            if (point_distance(cursorX, cursorY, foundInst.x, foundInst.y) > cursorMaxDistance){
                foundInst = noone;
            } else {
                if (ds_list_size(objPlayerCharacter.connecteds) == 1 && objPlayerCharacter.connectedMode == 1)
                    foundInst = noone;
                else
                    objPlayerCharacter.connectedMode = 0;
            }
        if (foundInst == noone){
            foundInst = instance_nearest(cursorX, cursorY, parEnemy)
            if (foundInst != noone)
                if (point_distance(cursorX, cursorY, foundInst.x, foundInst.y) > cursorMaxDistance){
                    foundInst = noone;
                } else {
                    if (ds_list_size(objPlayerCharacter.connecteds) == 1 && objPlayerCharacter.connectedMode == 0)
                        foundInst = noone;
                    else
                        objPlayerCharacter.connectedMode = 1;
                }
        }
        if (foundInst != noone){
        
            show_debug_message("list size:")
            show_debug_message(ds_list_size(objPlayerCharacter.connecteds));
        
            if (ds_list_size(objPlayerCharacter.connecteds) > 0 && objPlayerCharacter.connecteds[| 0] == foundInst){
                //exit;
            }
            if (ds_list_size(objPlayerCharacter.connecteds) > 1 && objPlayerCharacter.connecteds[| 1] == foundInst){
                //exit;
            }
        
            if (ds_list_size(objPlayerCharacter.connecteds) == 2){
            
                show_debug_message("NOOOOONN")
            
                //limit the connection target number to 2.
                //exit;
                connecteds[| 0].connected = false;
                connecteds[| 1].connected = false;
                ds_list_clear(objPlayerCharacter.connecteds);
                with (objConnectionTarget)
                    instance_destroy();
            }
            if (ds_list_size(objPlayerCharacter.connecteds) <= 1){
                foundInst.connected = true;
                PlaySound(snd_object_connected);
                ds_list_add(objPlayerCharacter.connecteds, foundInst);
                if (objPlayerCharacter.connectedMode == 0)
                    instance_create(foundInst.x + foundInst.sprite_width / 2 ,foundInst.y + foundInst.sprite_height / 2, objConnectionTarget);
                else
                    instance_create(foundInst.x,foundInst.y - foundInst.sprite_height / 2, objConnectionTarget);
            }
        }

    }
    
} else if (current_ability = 3) {

    //reset target flag
    objDrone.cFlag = false;

    //else, reset the position of cursor
    shouldDrawSelectionCursor = false;
    if (!controllingDrone){
        cursorX = objPlayerCharacter.x;
        cursorY = objPlayerCharacter.y;
    } else {
        cursorX = objDrone.x;
        cursorY = objDrone.y;
    }
    if (instance_exists(parEnemy))
        parEnemy.canMove = true;
    isSelecting = false;

    //global.droneState = 1;
    
    //destroy the targets
    with (objConnectionTarget)
        instance_destroy();
    
    //End of the selection, we launch the darts.
    if (!objPlayerCharacter.existingDarts){
        objPlayerCharacter.travellingDarts = ds_list_size(objPlayerCharacter.connecteds);
        
        //instantiate a dart for each target, and set the coordinates
        for (var i = 0; i < ds_list_size(objPlayerCharacter.connecteds); i++){
            var _d = instance_create(objDrone.x, objDrone.y, objDart);
            objPlayerCharacter.existingDarts = true;
            ds_list_add(objPlayerCharacter.darts, _d);
            with (_d){
                targetID = objPlayerCharacter.connecteds[| i].id;
                spd = 6;
            }
        }
    }
    
    
}
