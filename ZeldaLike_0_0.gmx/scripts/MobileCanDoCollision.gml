/// MobileCanDoCollision(hSpeed, vSpeed, obstacle, obj);
//
// Basically the same as MobileDoCollision, except that it does not move the object,
// and returns a boolean:
// True if the object can move
// False if not.
//
//******************************

// Get stuff
var _hSpeed, _vSpeed, _obstacle, _obj;
_hSpeed   = argument0;
_vSpeed   = argument1;
_obstacle = argument2;
_obj      = argument3;

var fakeX = _obj.x;
var fakeY = _obj.y;

// Do collision
var _i, _move;
for (_i=abs(_hSpeed); _i>0; _i--)
    {
        _move = sign(_hSpeed) * _i;
        if !place_meeting(fakeX+_move, fakeY,    _obstacle) {fakeX += _move; break;}
        if !place_meeting(fakeX+_move, fakeY-_i, _obstacle) {fakeY -= _i/2;}
        if !place_meeting(fakeX+_move, fakeY+_i, _obstacle) {fakeY += _i/2;}
    }
for (_i=abs(_vSpeed); _i>0; _i--)
    {
        _move = sign(_vSpeed) * _i;
        if !place_meeting(fakeX,    fakeY+_move, _obstacle) {fakeY += _move; break;}
        if !place_meeting(fakeX-_i, fakeY+_move, _obstacle) {fakeX -= _i/2;}
        if !place_meeting(fakeX+_i, fakeY+_move, _obstacle) {fakeX += _i/2;}
    }

// This is a fix for if the character wasn't able to move at least 1 whole pixel
if (abs(fakeX-xprevious) < 1)
&& (abs(fakeY-yprevious) < 1)
    {
        fakeX = xprevious;
        fakeY = yprevious;
    }

if (fakeX == _obj.x && fakeY == _obj.y){
    show_debug_message("Cannot move");
    return false;
}
show_debug_message("Can move");
return true;
