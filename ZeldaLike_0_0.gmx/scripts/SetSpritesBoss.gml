/// SetSpritesMuddy();
//
//*****************************************************************************

spriteIdleNorth    = sprBossIdleSouth;
spriteIdleSouth    = sprBossIdleSouth;
spriteIdleEast     = sprBossIdleSouth;
spriteIdleWest     = sprBossIdleSouth;

spriteWalkNorth    = sprBossIdleSouth;
spriteWalkSouth    = sprBossIdleSouth;
spriteWalkEast     = sprBossIdleSouth;
spriteWalkWest     = sprBossIdleSouth;

spriteAttackNorth  = sprBossIdleSouth;
spriteAttackSouth  = sprBossIdleSouth;
spriteAttackEast   = sprBossIdleSouth;
spriteAttackWest   = sprBossIdleSouth;

spriteEscapeNorth  = sprBossIdleSouth;
spriteEscapeSouth  = sprBossIdleSouth;
spriteEscapeEast   = sprBossIdleSouth;
spriteEscapeWest   = sprBossIdleSouth;

spriteStaggerNorth = sprBossIdleSouth;
spriteStaggerSouth = sprBossIdleSouth;
spriteStaggerEast  = sprBossIdleSouth;
spriteStaggerWest  = sprBossIdleSouth;

spriteDeathBits    = -1;
spriteDeathNorth   = sprForestMonsterDieNorth;
spriteDeathSouth   = sprForestMonsterDieSouth;
spriteDeathEast    = sprForestMonsterDieEast;
spriteDeathWest    = sprForestMonsterDieWest;
avatarSprite       = sprAvatarSnowScorpion; //needs to be added

// Set initial sprite
sprite_index = spriteIdleSouth;
