/*
//  Basically the same as MobileDoCollisionPushing, except that
//  we don't check if a collider is colliding. Hihi. Used in objPlayerCharacter/Step/ChargeUpdateCollider
*/

// Get stuff
var _hSpeed, _vSpeed, _obstacle;
_hSpeed   = argument0;
_vSpeed   = argument1;
_obstacle = argument2;

var _hSign = sign(_hSpeed);
var _vSign = sign(_vSpeed);

var _collider;


//Avoid pushing rocks in diagonal
if (abs(_hSpeed) > abs(_vSpeed)){
    _vSpeed = 0;
}
else{
    _hSpeed = 0;
}

//If we get a collision...
//if (place_meeting(x + _hSpeed, y + _vSpeed, _obstacle)){
if (1){

    //reduce player speed
    //objPlayerCharacter.mySpeed = 1.5;

    //Store the collider
    _collider = id;
    
    
    //Used to check if both of the connected objects can be moved.
    var _checkA = false;
    var _checkB = false;
    
    //Check if the collider is one of the connected object
    if (_collider == objPlayerCharacter.connecteds[| 0]){
    
        with(objPlayerCharacter.connecteds[| 1]){
            _checkA = MobileCanDoCollision(_hSpeed, _vSpeed, parObstaclePlayer, id);
        }
        with(_collider){
            _checkB = MobileCanDoCollision(_hSpeed, _vSpeed, parObstaclePlayer, id);
        }
    
        if (_checkA && _checkB){
            with(objPlayerCharacter.connecteds[| 1]){
                MobileDoCollision(_hSpeed, _vSpeed, parObstaclePlayer);
            }
            with(_collider){
                MobileDoCollision(_hSpeed, _vSpeed, parObstaclePlayer);
            }
        }
    }
    //Check if the collider is one of the connected object
    else if (_collider == objPlayerCharacter.connecteds[| 1]){
    
        with(objPlayerCharacter.connecteds[| 0]){
            _checkA = MobileCanDoCollision(_hSpeed, _vSpeed, parObstaclePlayer, id);
        }
        with(_collider){
            _checkB = MobileCanDoCollision(_hSpeed, _vSpeed, parObstaclePlayer, id);
        }
        
        if (_checkA && _checkB){
            with(objPlayerCharacter.connecteds[| 0]){
                MobileDoCollision(_hSpeed, _vSpeed, parObstaclePlayer);
            }
            with(_collider){
                MobileDoCollision(_hSpeed, _vSpeed, parObstaclePlayer);
            }
        }
    }
    //Else he's not connected, simple collision detection.
    else {
        with(_collider){
            MobileDoCollision(_hSpeed, _vSpeed, parObstaclePlayer);
        }
    }

} else {
    //objPlayerCharacter.mySpeed = 3;
}
