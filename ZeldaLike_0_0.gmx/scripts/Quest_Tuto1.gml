
// Setup
QuestNew("Tuto1", "Main Quest");
QuestGiver(objChirurgien);

// List requisites

// Start text
QuestDialogueContext(Context.intro);
QuestDialogueBegin(0);
QuestDialogue("Ah te voilà! Je t'attendais.");
QuestDialogueResponse(1,"Salut, tu m'attendais pour quoi?")
QuestDialogueBegin(1);
QuestDialogue("Premièrement: comment te sens tu?");
QuestDialogueResponse(2,"Pas trop mal, j'ai des douleurs aux articulations de... mon bras...")
QuestDialogueBegin(2);
QuestDialogue("Montre moi ton bras...");
QuestDialogue("...");
QuestDialogue("Je pense que tes douleurs sont liées à une inflammation de tes articulations qui grandissent, sans doute en décalé...");
QuestDialogue("Je peux te donner des anti-inflammatoires et des antalgiques si tu veux...");
QuestDialogueResponse(3, "Non je n'aime pas les antalgiques, je préfère être averti à la douleur...");
QuestDialogueBegin(3);
QuestDialogue("Vraiment? Bon si tu changes d'avis fais moi signe.");
QuestDialogue("Deuxièmement: la commandante te cherche, elle est dehors et elle t'attend. Ne la fais pas traîner.");
QuestDialogueAccept(4, 5, "D'accord je vais voir notre chère commandante.", "J'irai la voir quand j'aurais le temps.");
QuestDialogueBegin(4);
QuestDialogue("Bien, je retourne travailler, fais moi signe si tu veux discuter.");
QuestDialogueBegin(5);
QuestDialogue("Ne lui fais pas perdre patience.");

// Progress text
QuestDialogueContext(Context.progress);
QuestDialogueBegin(0);
QuestDialogue("Ne traîne pas, va parler à notre commandante.");

// End text


// Finish conditions
QuestCondition(CheckTalkToNpc, objCommandante);
//QuestCondition(EffectFinishQuest);

// Effects

