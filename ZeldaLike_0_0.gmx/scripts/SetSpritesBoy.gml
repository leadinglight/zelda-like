/// SetSpritesBoy();
//
//*****************************************************************************

spriteIdleNorth    = sprPlayerIdleNorth;
spriteIdleSouth    = sprPlayerIdleSouth;
spriteIdleEast     = sprPlayerIdleEast;
spriteIdleWest     = sprPlayerIdleWest;

spriteWalkNorth    = sprPlayerWalkNorth;
spriteWalkSouth    = sprPlayerWalkSouth;
spriteWalkEast     = sprPlayerWalkEast;
spriteWalkWest     = sprPlayerWalkWest;

spriteAttackNorth  = sprPlayerAttackNorth1;
spriteAttackSouth  = sprPlayerAttackSouth1;
spriteAttackEast   = sprPlayerAttackEast1;
spriteAttackWest   = sprPlayerAttackWest1;

spriteStaggerNorth = sprBoyStaggerNorth;
spriteStaggerSouth = sprBoyStaggerSouth;
spriteStaggerEast  = sprBoyStaggerEast;
spriteStaggerWest  = sprBoyStaggerWest;

spriteDefendStaggerNorth = sprPlayerDefendStaggerNorth;
spriteDefendStaggerSouth = sprPlayerDefendStaggerSouth;
spriteDefendStaggerEast  = sprPlayerDefendStaggerEast;
spriteDefendStaggerWest  = sprPlayerDefendStaggerWest;

spriteRootNorth = sprPlayerRootNorth;
spriteRootSouth = sprPlayerRootSouth;
spriteRootEast  = sprPlayerRootEast;
spriteRootWest  = sprPlayerRootWest;


spriteDeathBits    = sprDeathBones;
avatarSprite       = sprAvatarPlayerBoy;

// Set initial sprite
sprite_index = spriteIdleSouth;
