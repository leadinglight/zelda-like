/// SetSpritesMuddy();
//
//*****************************************************************************

spriteIdleNorth    = sprMuddyIdleNorth;
spriteIdleSouth    = sprMuddyIdleSouth;
spriteIdleEast     = sprMuddyIdleEast;
spriteIdleWest     = sprMuddyIdleWest;

spriteWalkNorth    = sprMuddyWalkNorth;
spriteWalkSouth    = sprMuddyWalkSouth;
spriteWalkEast     = sprMuddyWalkEast;
spriteWalkWest     = sprMuddyWalkWest;

spriteAttackNorth  = sprMuddyAttackNorth;
spriteAttackSouth  = sprMuddyAttackSouth;
spriteAttackEast   = sprMuddyAttackEast;
spriteAttackWest   = sprMuddyAttackWest;

spriteEscapeNorth  = sprMuddyEscapeNorth;
spriteEscapeSouth  = sprMuddyEscapeSouth;
spriteEscapeEast   = sprMuddyEscapeEast;
spriteEscapeWest   = sprMuddyEscapeWest;

spriteStaggerNorth = sprMuddyStaggerNorth;
spriteStaggerSouth = sprMuddyStaggerSouth;
spriteStaggerEast  = sprMuddyStaggerEast;
spriteStaggerWest  = sprMuddyStaggerWest;

spriteOutOfMudNorth= sprMuddyOutOfMudNorth;
spriteOutOfMudSouth= sprMuddyOutOfMudSouth;
spriteOutOfMudWest = sprMuddyOutOfMudWest;
spriteOutOfMudEast = sprMuddyOutOfMudEast;

spriteDeathBits    = -1;
spriteDeathNorth   = sprMuddyDeathNorth;
spriteDeathSouth   = sprMuddyDeathSouth;
spriteDeathEast    = sprMuddyDeathEast;
spriteDeathWest    = sprMuddyDeathWest;
avatarSprite       = sprAvatarSnowScorpion; //needs to be added

// Set initial sprite
sprite_index = spriteIdleSouth;
