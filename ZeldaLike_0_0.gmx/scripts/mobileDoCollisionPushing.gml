/// MobileDoCollisionPushing(hSpeed, vSpeed, obstacle);
//
//  Same as MobileDoCollision, but only compute collision with pushable objects.
//  You should use this after a MobileDoCollision, as this one will not modify v_spd and h_spd on the parent.
//  It will only modify coordinates of the collision object (if he's movable)
//
//*****************************************************************************

// Get stuff
var _hSpeed, _vSpeed, _obstacle;
_hSpeed   = argument0;
_vSpeed   = argument1;
_obstacle = argument2;

var _hSign = sign(_hSpeed);
var _vSign = sign(_vSpeed);

var _collider;


//Avoid pushing rocks in diagonal
if (abs(_hSpeed) > abs(_vSpeed)){
    _vSpeed = 0;
}
else{
    _hSpeed = 0;
}

//If we get a collision...
if (place_meeting(x + _hSpeed, y + _vSpeed, _obstacle)){
    PlaySound(pushCube);

    //reduce player speed
    objPlayerCharacter.mySpeed = 1.5;
    objDrone.spd = 2;

    //Store the collider
    _collider = instance_place(x + _hSpeed, y + _vSpeed, _obstacle);
    
    if (objPlayerCharacter.chargingWithDrone){
        objPlayerCharacter.chargingWithDrone = false;
        objPlayerCharacter.inAnimation = false;
        with (objPlayerCharacter){
            //Bump player backwards.
            MobileDoCollision(-_hSpeed * 2, -_vSpeed * 2, parObstaclePlayer);
        }
        //we should not be able to push an unpushable object with charge.
        if !_collider.pushable
            exit;
        
        if (object_get_name(_collider.object_index) != "objRocherBoss"){
            objPlayerCharacter.shouldUpdateCollider = true;
            objPlayerCharacter.alarm[5] = 0.8 * room_speed;
            global.colliderToUpdateInitialX = _collider.x;
            global.colliderToUpdateInitialY = _collider.y;
            global.colliderToUpdate = _collider;
            global.colliderToUpdateHSpeed = _hSpeed;
            global.colliderToUpdateVSpeed = _vSpeed;
        }
        
        //ScreenShake
        if (!instance_exists(objScreenShake)){
            var _newShake = instance_create(x, y, objScreenShake);
            _newShake.shake_amount = 10;
            _newShake.duration = 4;
            _newShake.alarm[0] = _newShake.duration;
        }
    }
    
    //Used to check if both of the connected objects can be moved.
    var _checkA = false;
    var _checkB = false;
    
    //Check if the collider is one of the connected object
    if (_collider == objPlayerCharacter.connecteds[| 0]){
    
        with(objPlayerCharacter.connecteds[| 1]){
            _checkA = MobileCanDoCollision(_hSpeed, _vSpeed, parObstaclePlayer, id);
        }
        with(_collider){
            _checkB = MobileCanDoCollision(_hSpeed, _vSpeed, parObstaclePlayer, id);
        }
    
        if (_checkA && _checkB){
            with(objPlayerCharacter.connecteds[| 1]){
                MobileDoCollision(_hSpeed, _vSpeed, parObstaclePlayer);
            }
            with(_collider){
                MobileDoCollision(_hSpeed, _vSpeed, parObstaclePlayer);
            }
        }
    }
    //Check if the collider is one of the connected object
    else if (_collider == objPlayerCharacter.connecteds[| 1]){
    
        with(objPlayerCharacter.connecteds[| 0]){
            _checkA = MobileCanDoCollision(_hSpeed, _vSpeed, parObstaclePlayer, id);
        }
        with(_collider){
            _checkB = MobileCanDoCollision(_hSpeed, _vSpeed, parObstaclePlayer, id);
        }
        
        if (_checkA && _checkB){
            with(objPlayerCharacter.connecteds[| 0]){
                MobileDoCollision(_hSpeed, _vSpeed, parObstaclePlayer);
            }
            with(_collider){
                MobileDoCollision(_hSpeed, _vSpeed, parObstaclePlayer);
            }
        }
    }
    //Else he's not connected, simple collision detection.
    else {
        with(_collider){
            MobileDoCollision(_hSpeed, _vSpeed, parObstaclePlayer);
        }
    }

} else {
    if (instance_exists(objPlayerCharacter))
        objPlayerCharacter.mySpeed = 3;
    objDrone.spd = 4;
}
