/// DrawAbilities (x, y)

// Position
var _x, _y;
_x = argument0;
_y = argument1;

if (objPlayerCharacter.current_ability = 1){
    /*if(objPlayerCharacter.cdStase>0){
        draw_sprite(sprStaseIconOff, 0, _x, _y);
        draw_sprite_part(sprStaseIconOn, 0, 0, 0, 61-(61/330*objPlayerCharacter.cdStase), 66, _x-30, _y-33);
    }
    else{
        draw_sprite(sprStaseIconOn, 0, _x, _y);
    }    */
    draw_sprite(sprStaseIconOn, 0, _x, _y);
}
else
    draw_sprite(sprStaseIconOff, 0, _x, _y);
    
//DRAW CD STASE    -------------------------------------------------
if(objPlayerCharacter.cdStase>0)
    draw_set_color(c_red);
else
    draw_set_color(c_green);      
draw_rectangle(_x-29, _y+40, _x+(58-58/310*objPlayerCharacter.cdStase-29), _y+57, false);
draw_set_color(c_gray);
draw_rectangle(_x-29, _y+40, _x+29, _y+57, true);
draw_set_color(c_white);
    
//------------------------------------------------------------------
     
_x += 64;
    
if (objPlayerCharacter.current_ability = 3)
    draw_sprite(sprConnectionIconOn, 0, _x, _y);
else
    draw_sprite(sprConnectionIconOff, 0, _x, _y);
