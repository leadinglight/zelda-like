var mouseX = mouse_x;
var mouseY = mouse_y;


var target = instance_nearest(objDrone.x, objDrone.y, parEnemy);

if (target){
    if (point_distance(objDrone.x, objDrone.y, target.x, target.y) < 300){
            //draw_sprite(sprTarget, 0, target.x, target.y);
            global.droneTarget = target;
            with (target){
            /*
                u_ht = shader_get_uniform(shdOutline,"ht");
                u_width = shader_get_uniform(shdOutline,"width");
                shader_set(shdOutline);
                shader_set_uniform_f(u_ht, 1/sprite_height);
                shader_set_uniform_f(u_width, 1/sprite_width);
                //draw_sprite_ext(sprite_index, 0, x, y, facing, 1, 0, -1, 1);
                shader_reset();
                //draw_self();
                //shader_reset();
                */
                
                draw_set_blend_mode(bm_add);
                draw_self();
                draw_set_blend_mode(bm_normal);
            }
        }
}
draw_self();
