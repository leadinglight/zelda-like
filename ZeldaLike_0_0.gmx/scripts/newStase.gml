/// New stase, instantiate a growing stase object t freeze every mob in the zone

if(objPlayerCharacter.current_ability == 1 && objPlayerCharacter.cdStase<=0){
    if(gamepad_is_connected(0))
        var _pressed = (gamepad_button_check_pressed(0, CONT_ABILITY));
    else
        var _pressed = (keyboard_check_pressed(KEY_ABILITY));
    
    if (_pressed){
        instance_create(objDrone.x, objDrone.y, objStaseZone);
        objPlayerCharacter.cdStase = 310;
    }
}
