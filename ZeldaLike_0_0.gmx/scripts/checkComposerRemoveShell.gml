//checks if the hitted enemy is a composer and remove its shell

if(object_index==objComposer){
    if(shieldOn = true){
        //Remove composer shell if we are charging
        if (objPlayerCharacter.chargingWithDrone){
            show_debug_message("REMOVED COMPOSER SHELL");
            shieldOn = false;
            beenHit = true;
            state = STATE_STAGGER;
            stateSwitched = true;
            staggerTimer = 40;
            knockback = true;            
            losingShield = true; 
            notComposerWithShield = false;   
        }
        else{
            //INSÉRER UN FX DE "LOUL MÊME PAS MAL", EN ATTENDANT GO FX DUST
            with(instance_create(x, y, objFXDust)){creator = other.id;}
        }    
        notComposerWithShield = false;
    }
}
