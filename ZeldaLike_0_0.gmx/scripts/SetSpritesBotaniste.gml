/// SetSpritesBotaniste();
//
//*****************************************************************************

spriteIdleNorth    = sprBotanisteIdleNorth;
spriteIdleSouth    = sprBotanisteIdleSouth;
spriteIdleEast     = sprBotanisteIdleEast;
spriteIdleWest     = sprBotanisteIdleWest;

spriteWalkNorth    = sprBotanisteWalkNorth;
spriteWalkSouth    = sprBotanisteWalkSouth;
spriteWalkEast     = sprBotanisteWalkEast;
spriteWalkWest     = sprBotanisteWalkWest;

// Set initial sprite
sprite_index = spriteIdleSouth;
