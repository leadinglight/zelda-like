// Setup
QuestNew("Quest Tuto Militaire", "Tuto Militaire");
QuestGiver(objMilitaire);

// List requisites

// Start text
QuestDialogueContext(Context.intro);
QuestDialogueBegin(0);
QuestDialogue("Ah te voilà! Tu veux que je te rappelle comment faire face au danger?");
QuestDialogueAccept(1,2,"Pourquoi pas.","Non merci, ça va aller.")

QuestDialogueBegin(1);
QuestDialogue("Tu vas bientôt entrer dans une zone inconnue qui risque d'être dangereuse. Tu vas devoir utiliser ton bras pour te défendre.");
QuestDialogue("(Attaquez avec le bouton X, maintenez X pour effectuer une attaque puissante. Appuyez sur le bouton A pour effectuer une esquive.)");
QuestDialogue("Tu vas aussi avoir besoin de ton drone et de ses capacités pour te sortir de certaines situations délicates.");
QuestDialogue("(Appuyez sur Y pour changer de capacité. La stase vous permet de geler des ennemis dans une zone autour de votre drone.)");
QuestDialogue("(La connexion vous permet de connecter deux objets ou deux ennemis entre eux, tout effet appliqué à l'un est appliqué à l'autre.)");
QuestDialogue("(Pressez la gachette droite RT pour utiliser la stase.)");
QuestDialogue("(Pour utiliser la connexion, maintenez RT enfoncée et sélectionnez les deux objets à connecter avec la touche X)");
QuestDialogue("Tu peux aussi utiliser les réacteurs de ton drone pour effectuer un choc assez fort pour par exemple déplacer certains objets lourds.");
QuestDialogue("(Maintenez A pour utiliser les réacteurs de votre drone.)");
QuestDialogue("Tu peux aussi piloter ton drone à distance pour l'envoyer en éclaireur et accéder à des zones qui te sont bloquées.");
QuestDialogue("(Appuyez sur la flèche directionnelle HAUT pour piloter votre drone.)");
QuestDialogue("Tu t'en souviendras?");
QuestDialogueResponse(3,"Facile.")
QuestDialogueResponse(4,"Pardon je regardais cette bestiole là-bas, tu peux répéter?");

QuestDialogueBegin(2);
QuestDialogue("Comme tu veux, mais évite d'être trop confiant.");

QuestDialogueBegin(3);
QuestDialogue("Le passage est bloqué mais regarde, il y a deux interrupteurs là-bas. Utilise les réacteurs de ton drone pour pousser ces gros blocs jusqu'à eux.");

QuestDialogueBegin(4);
QuestDialogue("Bon voilà un résumé:");
QuestDialogue("(Appuyez sur X pour attaquer, maintenez X pour effectuer une attaque puissante.)");
QuestDialogue("(Appuyez sur Y pour changer de capacité, appuyez sur la gachette droite RT pour utiliser la capacité sélectionnée.)");
QuestDialogue("(Appuyez sur A pour esquiver, maintenez A pour utiliser les réacteurs de votre drone.)");
QuestDialogue("(Appuyez sur la flèche directionnelle HAUT pour piloter votre drone à distance.)");
QuestDialogueResponse(5,"Merci, et maintenant?");

QuestDialogueBegin(5);
QuestDialogue("Le passage est bloqué mais regarde, il y a deux interrupteurs là-bas. Utilise les réacteurs de ton drone pour pousser ces gros blocs jusqu'à eux.");

// Progress text


// End text

// Finish conditions


// Effects
QuestEffect(EffectFinishQuest);
