///BossThrowRock()

timerThrowRock+=(1*bulletTime);
image_speed = 0.15*bulletTime;

if(timerThrowRock<=2){
    sprite_index = sprBossThrowRock;
    image_index = 0;
}


if(timerThrowRock>=66){
    var _direction = point_direction(x, y, player.x, player.y);
    //Envoyer Rocher Lent
    PlaySound(snd_throw_boss);
    with(instance_create(x+36, y-70, objRocherBoss)){
        direction = _direction;
        speed = 7;
        unstoppable = true;
        creator = objBoss;
        if(!other.firstRockThrown){
            other.firstRockThrown = true;
            firstRock = true;
            Debug(";)");
        }
    }
    currentAttack = BossWaiting;
    alarm[0] = cdThrowRock;
    timerThrowRock = 0;
    chancesToThrowRock = 2;
}
