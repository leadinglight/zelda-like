///BossHitByRock()

if(sprite_index!=sprBossTouchedByRock && sprite_index!=sprBossFaiblesseOut 
&& sprite_index!=sprBossGlandeDurcieBack && sprite_index!=sprBossGlandeBack
&& sprite_index!=sprBossThrowingGlande && sprite_index!=sprBossThrowingGlandeDurcie
&& sprite_index!=sprBossFaiblesseOut && sprite_index!=sprBossFaiblesseOutDurcie){
    sprite_index = sprBossTouchedByRock;
    image_index = 0;
    timerHitByRock = 1;
}

//ANIM TOUCHÉ----------------
if(timerHitByRock==0){    
    sprite_index = sprBossTouchedByRock;
    image_index = 0;
}

if(sprite_index == sprBossTouchedByRock && round(image_index)==image_number){
    if(bossState<=1)
        sprite_index = sprBossThrowingGlande;
    else
        sprite_index = sprBossThrowingGlandeDurcie;
    image_index = 0;
}

//FAIBLESSE OUT---------------------
if((sprite_index == sprBossThrowingGlande || sprite_index == sprBossThrowingGlandeDurcie)&& round(image_index)==image_number){
    if(bossState<=1)
        sprite_index = sprBossFaiblesseOut;
    else
        sprite_index = sprBossFaiblesseOutDurcie;
    image_index = 0;
    
    with(objFaiblesseBoss){            
        inBoss = false;
        x = positionOutBossX;
        y = positionOutBossY;
        alarm[0] = 240;
    }
}

//INCRÉMENTATION
timerHitByRock+=1*bulletTime;
image_speed = 0.15*bulletTime;

//FIN DU HITBYROCK
if(timerHitByRock>=240 && sprite_index!=sprBossGlandeDurcieBack && sprite_index!=sprBossGlandeBack){
    if(bossState<=1){
        sprite_index = sprBossGlandeBack;
        image_index = 0;
    }
    else{
        sprite_index = sprBossGlandeDurcieBack;
        image_index = 0;
    }
    Debug("sprite_index : " + string(sprite_index));
}

if((sprite_index==sprBossGlandeDurcieBack || sprite_index==sprBossGlandeBack) && round(image_index)==image_number){
    Debug("fin");
    with(objFaiblesseBoss){
        inBoss=true;
        x = positionInBossX;
        y = positionInBossY;
    }
    alarm[0] = 60;
    currentAttack = BossWaiting;
    timerHitByRock = 0;
}
