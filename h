[1mdiff --git a/ZeldaLike_0_0.gmx/extensions/Geon FX - Particle Editor.extension.gmx b/ZeldaLike_0_0.gmx/extensions/Geon FX - Particle Editor.extension.gmx[m
[1mindex 0b27209..5122d7a 100644[m
[1m--- a/ZeldaLike_0_0.gmx/extensions/Geon FX - Particle Editor.extension.gmx[m	
[1m+++ b/ZeldaLike_0_0.gmx/extensions/Geon FX - Particle Editor.extension.gmx[m	
[36m@@ -17,7 +17,12 @@[m
   <androidsourcedir></androidsourcedir>[m
   <macsourcedir></macsourcedir>[m
   <maclinkerflags></maclinkerflags>[m
[32m+[m[32m  <maccompilerflags></maccompilerflags>[m
   <androidinject></androidinject>[m
[32m+[m[32m  <androidmanifestinject></androidmanifestinject>[m
[32m+[m[32m  <iosplistinject></iosplistinject>[m
[32m+[m[32m  <androidactivityinject></androidactivityinject>[m
[32m+[m[32m  <gradleinject></gradleinject>[m
   <iosSystemFrameworks/>[m
   <iosThirdPartyFrameworks/>[m
   <androidPermissions/>[m
[1mdiff --git a/ZeldaLike_0_0.gmx/fonts/font_menu.font.gmx b/ZeldaLike_0_0.gmx/fonts/font_menu.font.gmx[m
[1mindex 849f54a..b14cf60 100644[m
[1m--- a/ZeldaLike_0_0.gmx/fonts/font_menu.font.gmx[m
[1m+++ b/ZeldaLike_0_0.gmx/fonts/font_menu.font.gmx[m
[36m@@ -16,102 +16,102 @@[m
     <range0>32,127</range0>[m
   </ranges>[m
   <glyphs>[m
[31m-    <glyph character="113" x="179" y="2" w="8" h="18" shift="9" offset="1"/>[m
[31m-    <glyph character="87" x="2" y="2" w="13" h="17" shift="13" offset="0"/>[m
[31m-    <glyph character="50" x="169" y="23" w="7" h="17" shift="8" offset="1"/>[m
[31m-    <glyph character="108" x="41" y="64" w="3" h="17" shift="4" offset="1"/>[m
[31m-    <glyph character="61" x="102" y="43" w="8" h="13" shift="9" offset="1"/>[m
[31m-    <glyph character="77" x="93" y="2" w="10" h="17" shift="12" offset="1"/>[m
[31m-    <glyph character="56" x="196" y="23" w="7" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="32" x="20" y="64" w="3" h="20" shift="3" offset="0"/>[m
[32m+[m[32m    <glyph character="33" x="8" y="64" w="4" h="17" shift="5" offset="1"/>[m
[32m+[m[32m    <glyph character="34" x="66" y="64" w="6" h="8" shift="6" offset="1"/>[m
     <glyph character="35" x="69" y="2" w="10" h="17" shift="10" offset="1"/>[m
[31m-    <glyph character="115" x="11" y="43" w="7" h="17" shift="8" offset="1"/>[m
[31m-    <glyph character="122" x="136" y="43" w="6" h="17" shift="6" offset="0"/>[m
[31m-    <glyph character="105" x="46" y="64" w="3" h="17" shift="4" offset="1"/>[m
[32m+[m[32m    <glyph character="36" x="169" y="2" w="8" h="18" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="37" x="81" y="2" w="10" h="17" shift="11" offset="1"/>[m
[32m+[m[32m    <glyph character="38" x="116" y="2" w="9" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="39" x="84" y="64" w="3" h="8" shift="3" offset="1"/>[m
[32m+[m[32m    <glyph character="40" x="14" y="64" w="4" h="17" shift="5" offset="1"/>[m
[32m+[m[32m    <glyph character="41" x="242" y="43" w="4" h="17" shift="5" offset="1"/>[m
[32m+[m[32m    <glyph character="42" x="78" y="64" w="4" h="8" shift="5" offset="1"/>[m
[32m+[m[32m    <glyph character="43" x="92" y="43" w="8" h="14" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="44" x="30" y="64" w="3" h="18" shift="3" offset="1"/>[m
[32m+[m[32m    <glyph character="45" x="35" y="64" w="4" h="13" shift="5" offset="1"/>[m
[32m+[m[32m    <glyph character="46" x="51" y="64" w="3" h="17" shift="3" offset="1"/>[m
     <glyph character="47" x="38" y="43" w="7" h="17" shift="7" offset="0"/>[m
[31m-    <glyph character="80" x="47" y="43" w="7" h="17" shift="8" offset="1"/>[m
[31m-    <glyph character="82" x="29" y="43" w="7" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="125" x="162" y="43" w="5" h="19" shift="6" offset="1"/>[m
     <glyph character="48" x="214" y="23" w="7" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="32" x="20" y="64" w="3" h="20" shift="3" offset="0"/>[m
[31m-    <glyph character="91" x="2" y="64" w="4" h="17" shift="5" offset="1"/>[m
[32m+[m[32m    <glyph character="49" x="197" y="43" w="5" h="17" shift="6" offset="0"/>[m
[32m+[m[32m    <glyph character="50" x="169" y="23" w="7" h="17" shift="8" offset="1"/>[m
[32m+[m[32m    <glyph character="51" x="187" y="23" w="7" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="52" x="92" y="23" w="8" h="17" shift="8" offset="0"/>[m
[32m+[m[32m    <glyph character="53" x="56" y="43" w="7" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="54" x="232" y="23" w="7" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="55" x="128" y="43" w="6" h="17" shift="7" offset="0"/>[m
[32m+[m[32m    <glyph character="56" x="196" y="23" w="7" h="17" shift="9" offset="1"/>[m
     <glyph character="57" x="241" y="23" w="7" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="79" x="205" y="23" w="7" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="97" x="160" y="23" w="7" h="17" shift="8" offset="1"/>[m
[31m-    <glyph character="98" x="102" y="23" w="8" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="107" x="74" y="43" w="7" h="17" shift="8" offset="1"/>[m
[31m-    <glyph character="114" x="183" y="43" w="5" h="17" shift="6" offset="1"/>[m
[31m-    <glyph character="46" x="51" y="64" w="3" h="17" shift="3" offset="1"/>[m
[31m-    <glyph character="38" x="116" y="2" w="9" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="100" x="82" y="23" w="8" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="44" x="30" y="64" w="3" h="18" shift="3" offset="1"/>[m
[31m-    <glyph character="81" x="131" y="23" w="7" h="18" shift="9" offset="1"/>[m
[31m-    <glyph character="90" x="112" y="43" w="6" h="17" shift="7" offset="0"/>[m
[31m-    <glyph character="92" x="20" y="43" w="7" h="17" shift="7" offset="0"/>[m
[31m-    <glyph character="68" x="219" y="2" w="8" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="36" x="169" y="2" w="8" h="18" shift="9" offset="1"/>[m
[31m-    <glyph character="86" x="105" y="2" w="9" h="17" shift="8" offset="0"/>[m
[31m-    <glyph character="64" x="17" y="2" w="12" h="17" shift="13" offset="1"/>[m
[31m-    <glyph character="37" x="81" y="2" w="10" h="17" shift="11" offset="1"/>[m
[32m+[m[32m    <glyph character="58" x="61" y="64" w="3" h="17" shift="3" offset="1"/>[m
[32m+[m[32m    <glyph character="59" x="25" y="64" w="3" h="19" shift="3" offset="1"/>[m
[32m+[m[32m    <glyph character="60" x="150" y="23" w="8" h="15" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="61" x="102" y="43" w="8" h="13" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="62" x="140" y="23" w="8" h="15" shift="9" offset="1"/>[m
     <glyph character="63" x="239" y="2" w="8" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="64" x="17" y="2" w="12" h="17" shift="13" offset="1"/>[m
[32m+[m[32m    <glyph character="65" x="138" y="2" w="9" h="17" shift="8" offset="0"/>[m
[32m+[m[32m    <glyph character="66" x="32" y="23" w="8" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="67" x="12" y="23" w="8" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="68" x="219" y="2" w="8" h="17" shift="9" offset="1"/>[m
     <glyph character="69" x="144" y="43" w="6" h="17" shift="7" offset="1"/>[m
[32m+[m[32m    <glyph character="70" x="120" y="43" w="6" h="17" shift="7" offset="1"/>[m
[32m+[m[32m    <glyph character="71" x="22" y="23" w="8" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="72" x="112" y="23" w="8" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="73" x="56" y="64" w="3" h="17" shift="5" offset="1"/>[m
[32m+[m[32m    <glyph character="74" x="211" y="43" w="5" h="17" shift="6" offset="0"/>[m
[32m+[m[32m    <glyph character="75" x="229" y="2" w="8" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="76" x="204" y="43" w="5" h="17" shift="6" offset="1"/>[m
[32m+[m[32m    <glyph character="77" x="93" y="2" w="10" h="17" shift="12" offset="1"/>[m
[32m+[m[32m    <glyph character="78" x="83" y="43" w="7" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="79" x="205" y="23" w="7" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="80" x="47" y="43" w="7" h="17" shift="8" offset="1"/>[m
[32m+[m[32m    <glyph character="81" x="131" y="23" w="7" h="18" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="82" x="29" y="43" w="7" h="17" shift="9" offset="1"/>[m
     <glyph character="83" x="209" y="2" w="8" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="84" x="223" y="23" w="7" h="17" shift="8" offset="0"/>[m
[32m+[m[32m    <glyph character="85" x="52" y="23" w="8" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="86" x="105" y="2" w="9" h="17" shift="8" offset="0"/>[m
[32m+[m[32m    <glyph character="87" x="2" y="2" w="13" h="17" shift="13" offset="0"/>[m
[32m+[m[32m    <glyph character="88" x="127" y="2" w="9" h="17" shift="7" offset="-1"/>[m
     <glyph character="89" x="189" y="2" w="8" h="17" shift="8" offset="0"/>[m
[31m-    <glyph character="41" x="242" y="43" w="4" h="17" shift="5" offset="1"/>[m
[31m-    <glyph character="112" x="159" y="2" w="8" h="18" shift="9" offset="1"/>[m
[31m-    <glyph character="43" x="92" y="43" w="8" h="14" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="90" x="112" y="43" w="6" h="17" shift="7" offset="0"/>[m
[32m+[m[32m    <glyph character="91" x="2" y="64" w="4" h="17" shift="5" offset="1"/>[m
[32m+[m[32m    <glyph character="92" x="20" y="43" w="7" h="17" shift="7" offset="0"/>[m
[32m+[m[32m    <glyph character="93" x="248" y="43" w="4" h="17" shift="5" offset="1"/>[m
[32m+[m[32m    <glyph character="94" x="218" y="43" w="8" h="10" shift="8" offset="0"/>[m
[32m+[m[32m    <glyph character="95" x="58" y="2" w="9" h="19" shift="9" offset="0"/>[m
     <glyph character="96" x="89" y="64" w="4" h="5" shift="6" offset="0"/>[m
[31m-    <glyph character="104" x="199" y="2" w="8" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="111" x="65" y="43" w="7" h="17" shift="8" offset="1"/>[m
[31m-    <glyph character="120" x="2" y="23" w="8" h="17" shift="7" offset="0"/>[m
[31m-    <glyph character="126" x="152" y="43" w="8" h="12" shift="9" offset="1"/>[m
[31m-    <glyph character="33" x="8" y="64" w="4" h="17" shift="5" offset="1"/>[m
[32m+[m[32m    <glyph character="97" x="160" y="23" w="7" h="17" shift="8" offset="1"/>[m
[32m+[m[32m    <glyph character="98" x="102" y="23" w="8" h="17" shift="9" offset="1"/>[m
     <glyph character="99" x="178" y="23" w="7" h="17" shift="8" offset="1"/>[m
[31m-    <glyph character="73" x="56" y="64" w="3" h="17" shift="5" offset="1"/>[m
[31m-    <glyph character="106" x="236" y="43" w="4" h="18" shift="4" offset="0"/>[m
[31m-    <glyph character="88" x="127" y="2" w="9" h="17" shift="7" offset="-1"/>[m
[32m+[m[32m    <glyph character="100" x="82" y="23" w="8" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="101" x="2" y="43" w="7" h="17" shift="8" offset="1"/>[m
     <glyph character="102" x="190" y="43" w="5" h="17" shift="5" offset="0"/>[m
[32m+[m[32m    <glyph character="103" x="149" y="2" w="8" h="18" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="104" x="199" y="2" w="8" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="105" x="46" y="64" w="3" h="17" shift="4" offset="1"/>[m
[32m+[m[32m    <glyph character="106" x="236" y="43" w="4" h="18" shift="4" offset="0"/>[m
[32m+[m[32m    <glyph character="107" x="74" y="43" w="7" h="17" shift="8" offset="1"/>[m
[32m+[m[32m    <glyph character="108" x="41" y="64" w="3" h="17" shift="4" offset="1"/>[m
     <glyph character="109" x="31" y="2" w="12" h="17" shift="13" offset="1"/>[m
[31m-    <glyph character="118" x="62" y="23" w="8" h="17" shift="8" offset="0"/>[m
[31m-    <glyph character="67" x="12" y="23" w="8" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="101" x="2" y="43" w="7" h="17" shift="8" offset="1"/>[m
[31m-    <glyph character="121" x="122" y="23" w="7" h="18" shift="7" offset="0"/>[m
[31m-    <glyph character="74" x="211" y="43" w="5" h="17" shift="6" offset="0"/>[m
[31m-    <glyph character="84" x="223" y="23" w="7" h="17" shift="8" offset="0"/>[m
[31m-    <glyph character="39" x="84" y="64" w="3" h="8" shift="3" offset="1"/>[m
[31m-    <glyph character="94" x="218" y="43" w="8" h="10" shift="8" offset="0"/>[m
[31m-    <glyph character="59" x="25" y="64" w="3" h="19" shift="3" offset="1"/>[m
[31m-    <glyph character="49" x="197" y="43" w="5" h="17" shift="6" offset="0"/>[m
[31m-    <glyph character="51" x="187" y="23" w="7" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="54" x="232" y="23" w="7" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="66" x="32" y="23" w="8" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="60" x="150" y="23" w="8" h="15" shift="9" offset="1"/>[m
[31m-    <glyph character="71" x="22" y="23" w="8" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="95" x="58" y="2" w="9" h="19" shift="9" offset="0"/>[m
[32m+[m[32m    <glyph character="110" x="42" y="23" w="8" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="111" x="65" y="43" w="7" h="17" shift="8" offset="1"/>[m
[32m+[m[32m    <glyph character="112" x="159" y="2" w="8" h="18" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="113" x="179" y="2" w="8" h="18" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="114" x="183" y="43" w="5" h="17" shift="6" offset="1"/>[m
[32m+[m[32m    <glyph character="115" x="11" y="43" w="7" h="17" shift="8" offset="1"/>[m
     <glyph character="116" x="176" y="43" w="5" h="17" shift="5" offset="0"/>[m
[32m+[m[32m    <glyph character="117" x="72" y="23" w="8" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="118" x="62" y="23" w="8" h="17" shift="8" offset="0"/>[m
     <glyph character="119" x="45" y="2" w="11" h="17" shift="11" offset="0"/>[m
[31m-    <glyph character="40" x="14" y="64" w="4" h="17" shift="5" offset="1"/>[m
[31m-    <glyph character="65" x="138" y="2" w="9" h="17" shift="8" offset="0"/>[m
[31m-    <glyph character="110" x="42" y="23" w="8" h="17" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="120" x="2" y="23" w="8" h="17" shift="7" offset="0"/>[m
[32m+[m[32m    <glyph character="121" x="122" y="23" w="7" h="18" shift="7" offset="0"/>[m
[32m+[m[32m    <glyph character="122" x="136" y="43" w="6" h="17" shift="6" offset="0"/>[m
     <glyph character="123" x="169" y="43" w="5" h="19" shift="6" offset="1"/>[m
[31m-    <glyph character="127" x="228" y="43" w="6" h="12" shift="8" offset="1"/>[m
[31m-    <glyph character="55" x="128" y="43" w="6" h="17" shift="7" offset="0"/>[m
[31m-    <glyph character="53" x="56" y="43" w="7" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="62" x="140" y="23" w="8" h="15" shift="9" offset="1"/>[m
[31m-    <glyph character="45" x="35" y="64" w="4" h="13" shift="5" offset="1"/>[m
[31m-    <glyph character="42" x="78" y="64" w="4" h="8" shift="5" offset="1"/>[m
[31m-    <glyph character="52" x="92" y="23" w="8" h="17" shift="8" offset="0"/>[m
[31m-    <glyph character="93" x="248" y="43" w="4" h="17" shift="5" offset="1"/>[m
[31m-    <glyph character="103" x="149" y="2" w="8" h="18" shift="9" offset="1"/>[m
[31m-    <glyph character="78" x="83" y="43" w="7" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="70" x="120" y="43" w="6" h="17" shift="7" offset="1"/>[m
[31m-    <glyph character="76" x="204" y="43" w="5" h="17" shift="6" offset="1"/>[m
[31m-    <glyph character="117" x="72" y="23" w="8" h="17" shift="9" offset="1"/>[m
     <glyph character="124" x="74" y="64" w="2" h="19" shift="5" offset="2"/>[m
[31m-    <glyph character="34" x="66" y="64" w="6" h="8" shift="6" offset="1"/>[m
[31m-    <glyph character="72" x="112" y="23" w="8" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="75" x="229" y="2" w="8" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="85" x="52" y="23" w="8" h="17" shift="9" offset="1"/>[m
[31m-    <glyph character="58" x="61" y="64" w="3" h="17" shift="3" offset="1"/>[m
[32m+[m[32m    <glyph character="125" x="162" y="43" w="5" h="19" shift="6" offset="1"/>[m
[32m+[m[32m    <glyph character="126" x="152" y="43" w="8" h="12" shift="9" offset="1"/>[m
[32m+[m[32m    <glyph character="127" x="228" y="43" w="6" h="12" shift="8" offset="1"/>[m
   </glyphs>[m
   <kerningPairs/>[m
   <image>font_menu.png</image>[m
[1mdiff --git a/ZeldaLike_0_0.gmx/rooms/rmDL4.room.gmx b/ZeldaLike_0_0.gmx/rooms/rmDL4.room.gmx[m
[1mindex 8cba3da..7a67664 100644[m
[1m--- a/ZeldaLike_0_0.gmx/rooms/rmDL4.room.gmx[m
[1m+++ b/ZeldaLike_0_0.gmx/rooms/rmDL4.room.gmx[m
[36m@@ -121,7 +121,7 @@[m
     <instance objName="objPlatform" x="704" y="192" name="inst_638417D6" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
     <instance objName="objPlatform" x="160" y="288" name="inst_AC2ACA63" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
     <instance objName="objSmallGenerator" x="160" y="288" name="inst_C418875F" locked="0" code="targetY = 32 * 4;&#xA;&#xA;&#xA;" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[31m-    <instance objName="objMuddy" x="192" y="576" name="inst_11E513F0" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="objMuddy" x="288" y="640" name="inst_11E513F0" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
     <instance objName="obj_door_open_2" x="96" y="544" name="inst_687DFDAE" locked="0" code="" scaleX="0.5" scaleY="0.5" colour="4294967295" rotation="0"/>[m
     <instance objName="obj_door_open_1" x="960" y="128" name="inst_6BF91665" locked="0" code="" scaleX="-0.5" scaleY="0.5" colour="4294967295" rotation="0"/>[m
     <instance objName="obj_door_open_2" x="960" y="213" name="inst_ED879D2D" locked="0" code="" scaleX="-0.5" scaleY="0.5" colour="4294967295" rotation="0"/>[m
[36m@@ -183,7 +183,7 @@[m
     <instance objName="objWallSquareEnviro" x="800" y="288" name="inst_8601E1A5" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
     <instance objName="objWallSquareEnviro" x="768" y="288" name="inst_FDAA2C2D" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
     <instance objName="objFallTP" x="159" y="310" name="inst_E0872D1F" locked="0" code="fallTarget = inst_088EB2BD;&#xA;" scaleX="2" scaleY="5" colour="4294967295" rotation="0"/>[m
[31m-    <instance objName="objZoneMobs" x="128" y="480" name="inst_83565705" locked="0" code="" scaleX="4" scaleY="5" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="objZoneMobs" x="224" y="576" name="inst_83565705" locked="0" code="" scaleX="4" scaleY="2" colour="4294967295" rotation="0"/>[m
     <instance objName="control" x="512" y="448" name="inst_B940B047" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
     <instance objName="obj_Light_white" x="255" y="180" name="inst_1BE57FC7" locked="0" code="pl_light_init(50, make_color_rgb(255, 255, 255), 0.9);" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
     <instance objName="objLightBig" x="544" y="416" name="inst_EE8D1842" locked="0" code="&#xA;pl_light_init(820, make_color_rgb(182, 255, 204), 0.8);&#xA;" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[1mdiff --git a/ZeldaLike_0_0.gmx/rooms/rmDL5.room.gmx b/ZeldaLike_0_0.gmx/rooms/rmDL5.room.gmx[m
[1mindex 8f3327a..2910a20 100644[m
[1m--- a/ZeldaLike_0_0.gmx/rooms/rmDL5.room.gmx[m
[1m+++ b/ZeldaLike_0_0.gmx/rooms/rmDL5.room.gmx[m
[36m@@ -505,9 +505,14 @@[m
     <instance objName="objLightBig" x="1440" y="1248" name="inst_2900001E" locked="0" code="&#xA;pl_light_init(500, make_color_rgb(182, 255, 204), 0.70);&#xA;" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
     <instance objName="objLightBig" x="736" y="1248" name="inst_06344166" locked="0" code="&#xA;pl_light_init(500, make_color_rgb(182, 255, 204), 0.70);&#xA;" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
     <instance objName="objLightBig" x="288" y="800" name="inst_223992A4" locked="0" code="&#xA;pl_light_init(270, make_color_rgb(182, 255, 204), 0.80);&#xA;" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[31m-    <instance objName="obj_Light_white" x="505" y="1398" name="inst_6949A0DB" locked="0" code="&#xA;pl_light_init(30, make_color_rgb(255, 255, 255), 0.80);&#xA;" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[31m-    <instance objName="obj_Light_white" x="512" y="224" name="inst_7C0D60DD" locked="0" code="&#xA;pl_light_init(30, make_color_rgb(255, 255, 255), 0.80);&#xA;" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[31m-    <instance objName="obj_Light_white" x="1216" y="224" name="inst_6BB33D06" locked="0" code="&#xA;pl_light_init(30, make_color_rgb(255, 255, 255), 0.80);&#xA;" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="obj_Light_white" x="506" y="1398" name="inst_6949A0DB" locked="0" code="&#xA;pl_light_init(30, make_color_rgb(255, 255, 255), 0.80);&#xA;" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="obj_Light_white" x="504" y="224" name="inst_7C0D60DD" locked="0" code="&#xA;pl_light_init(30, make_color_rgb(255, 255, 255), 0.80);&#xA;" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="obj_Light_white" x="1208" y="224" name="inst_6BB33D06" locked="0" code="&#xA;pl_light_init(30, make_color_rgb(255, 255, 255), 0.80);&#xA;" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="obj_driedead1" x="128" y="640" name="inst_4D73A61C" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="obj_driedead1" x="128" y="736" name="inst_11A5A765" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="obj_driedead1" x="128" y="832" name="inst_D3DE9BC7" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="obj_driedead3" x="1216" y="608" name="inst_EEA0E8D5" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="obj_driedead3" x="992" y="608" name="inst_D111CF24" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
   </instances>[m
   <tiles>[m
     <tile bgName="bckDungeon" x="928" y="224" w="64" h="64" xo="0" yo="137" id="10098470" name="inst_1B16AF68" depth="1000000" locked="0" colour="4294967295" scaleX="1" scaleY="1"/>[m
[1mdiff --git a/ZeldaLike_0_0.gmx/rooms/rmVillage.room.gmx b/ZeldaLike_0_0.gmx/rooms/rmVillage.room.gmx[m
[1mindex 094775f..c54a6ac 100644[m
[1m--- a/ZeldaLike_0_0.gmx/rooms/rmVillage.room.gmx[m
[1m+++ b/ZeldaLike_0_0.gmx/rooms/rmVillage.room.gmx[m
[36m@@ -583,6 +583,17 @@[m
     <instance objName="objWallSquare" x="1600" y="1440" name="inst_5693311F" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
     <instance objName="objWallSquare" x="1632" y="1440" name="inst_82381A1E" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
     <instance objName="objWallSquare" x="1664" y="1440" name="inst_C00083FE" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="objWallSquare" x="1184" y="1440" name="inst_8415080C" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="objWallSquare" x="1216" y="1440" name="inst_CE803216" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="objWallSquare" x="1184" y="1452" name="inst_D3F3E40F" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="objWallSquare" x="1216" y="1452" name="inst_E9E84D76" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="objWallSquare" x="1248" y="1452" name="inst_CD942702" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="objWallSquare" x="1600" y="1449" name="inst_C25DAD4B" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="objWallSquare" x="1568" y="1449" name="inst_1F7A737A" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="objWallSquare" x="1632" y="1449" name="inst_FC2F745E" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="objWallSquare" x="1664" y="1449" name="inst_464DA56C" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="objWallSquare" x="480" y="1248" name="inst_982A59F1" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
[32m+[m[32m    <instance objName="objWallSquare" x="736" y="1248" name="inst_122F2C37" locked="0" code="" scaleX="1" scaleY="1" colour="4294967295" rotation="0"/>[m
   </instances>[m
   <tiles>[m
     <tile bgName="&lt;undefined&gt;" x="640" y="544" w="32" h="32" xo="96" yo="96" id="10090502" name="inst_96AA6137" depth="1000000" locked="0" colour="4294967295" scaleX="1" scaleY="1"/>[m
[1mdiff --git a/ZeldaLike_0_0.gmx/scripts/DrawLifeHearts.gml b/ZeldaLike_0_0.gmx/scripts/DrawLifeHearts.gml[m
[1mindex 68fc6ef..1d431c1 100644[m
[1m--- a/ZeldaLike_0_0.gmx/scripts/DrawLifeHearts.gml[m
[1m+++ b/ZeldaLike_0_0.gmx/scripts/DrawLifeHearts.gml[m
[36m@@ -33,4 +33,4 @@[m [mrepeat (hearts-life)[m
     {[m
     draw_sprite(sprLifeHeart, 1, _x, _y);[m
     _x += 36;[m
[31m-    }[m
\ No newline at end of file[m
[32m+[m[32m    }[m
